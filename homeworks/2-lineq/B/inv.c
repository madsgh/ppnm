#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>


// solve QRx = b by back-substitution
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);


// calculate inverse of A = Q*R into matrix B
void GS_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B) {
   
   // check sizes
   assert(Q->size1 == Q->size2 && R->size1 == R->size2 && B->size1 == B->size2 &&
          Q->size1 == R->size1 && R->size1 == B->size1 && 
          "Q, R and B must be square matrices of same size!");
   
   int n = Q->size1;

   // allocate vector ei for solving A*bi = ei
   gsl_vector* ei = gsl_vector_alloc(n);

   // loop over columns i
   for (int i = 0; i < n; ++i) { 

      gsl_vector_view view = gsl_matrix_column(B, i);
      gsl_vector* bi = &view.vector;
      gsl_vector_set_basis(ei, i);
      
      GS_solve(Q, R, ei, bi);
   }

   // free memory
   gsl_vector_free(ei);

}
