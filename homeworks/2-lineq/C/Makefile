CC = clang
CFLAGS = -Wall -O1 -std=gnu11
CFLAGS += $(shell gsl-config --cflags)
LDLIBS += $(shell gsl-config --libs)

all: timings_raw.png timings_lin.png
	@echo '===================='
	@echo '====   TASK C   ===='
	@echo '===================='
	@echo
	@echo 'Check .png plots in directory C! The gsl QR decomposition uses roughly 60 % of the time used by my implementation.'
	@echo 'The double-log plot suggest that the computational scaling is a bit steeper than O(N**3).'

timings_raw.png: timings_raw.ppl
	pyxplot $< 1> /dev/null 

timings_raw.ppl: timings.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "timings_raw.png"'				>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@           		
	@echo 'set xlabel "$$N$$"'								>> $@      		
	@echo 'set ylabel "$$t/\mathrm{\mu s}$$"'				>> $@     		
	@echo 'set title "Raw timings of QR decomposition"'	>> $@	
	@echo 'plot \\'													>> $@
	@echo ' "timings.txt" using 1:2 w l lw 1 lt 1 color black title "my implementation" 	\\'>> $@
	@echo ',"timings.txt" using 1:3 w l lw 1 lt 1 color red   title "gsl-linalg-QR-decomp" \\'>> $@

timings_lin.png: timings_lin.ppl
	pyxplot $< 1> /dev/null

timings_lin.ppl: timings.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "timings_lin.png"'				>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@           		
	@echo 'set xlabel "$$\ln(N)$$"'						>> $@      		
	@echo 'set ylabel "$$\ln(t/\mathrm{\mu s})$$"'		>> $@     		
	@echo 'set title "Linearized timings of QR decomposition"'	>> $@	
	@echo 'f(x) = a*x + b'													>> $@           		
	@echo 'g(x) = c*x + d'													>> $@           		
	@echo 'fit f() withouterrors "timings.txt" using (ln($$1)):(ln($$2)) via a,b'		>> $@           		
	@echo 'fit g() withouterrors "timings.txt" using (ln($$1)):(ln($$3)) via c,d'		>> $@           		
	@echo 'plot \\'																						>> $@
	@echo ' "timings.txt" using (ln($$1)):(ln($$2)) w l lw 1 lt 1 color black title "my implementation" 	 		\\'>> $@
	@echo ',f(x)  												w l lw 1 lt 3 color black title "$$y = %.2f x %.2f$$"%(a,b) \\'>> $@
	@echo ',"timings.txt" using (ln($$1)):(ln($$3)) w l lw 1 lt 1 color red   title "gsl-linalg-QR-decomp" 		\\'>> $@
	@echo ',g(x)  												w l lw 1 lt 3 color red   title "$$y = %.2f x %.2f$$"%(c,d) \\'>> $@

timings.txt: main
	./$<

main: main.o ../util.o ../A/gs.o

clean:
	$(RM) main *.o *.txt *.ppl *.pdf *.png ../util.o

test:
	@echo $(CFLAGS)
	@echo $(LDLIBS)
