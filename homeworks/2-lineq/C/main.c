#include<stdio.h>
#include<stdlib.h>
#include<sys/time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>
#include"../util.h"

// QR decomposition Gram-Schmidt algorithm (A <- Q)
void GS_decomp(gsl_matrix* A, gsl_matrix* R);

// stopwatch (ns)
long get_time_nsec() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((long)ts.tv_sec)*1000000000L + ts.tv_nsec;
}

// stopwatch (µs)
long get_time_usec() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((long)ts.tv_sec)*1000000L + ts.tv_nsec/1000L;
}

// stopwatch (ms)
long get_time_msec() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((long)ts.tv_sec)*1000L + ts.tv_nsec/1000000L;
}


int main() {

   // maximum matrix size
   int N = 600;
   gsl_matrix* A1;
   gsl_matrix* A2;
   gsl_matrix* R1;
   gsl_vector* R2;
   
   // open file to keep timings
   FILE* timings_file = fopen("timings.txt", "w");

   // loop over sizes
   for (int n = 75; n <= N; n += 25) {
      
      // allocate matrices
      A1 = gsl_matrix_alloc(n, n);
      A2 = gsl_matrix_alloc(n, n);
      R1 = gsl_matrix_alloc(n, n);
      R2 = gsl_vector_alloc(n);
      
      // fill out A with random numbers
      unsigned int SEED = time(NULL);
      srandom(SEED);
      for (int i = 0; i < A1->size1; ++i) {
         for (int j = 0; j < A1->size2; ++j) {
            gsl_matrix_set(A1, i, j, 5*((double)random())/RAND_MAX);
         }
      }
      
      // make copy of random matrix
      gsl_matrix_memcpy(A2, A1);

      // do timings
      long int start = get_time_usec(); 
      GS_decomp(A1, R1);
      long int stop = get_time_usec();
      fprintf(timings_file, "%10d %10ld ", n, stop-start);

      start = get_time_usec(); 
      gsl_linalg_QR_decomp(A2, R2);
      stop = get_time_usec();
      fprintf(timings_file, "%10ld\n", stop-start);

   
      // free memory
      gsl_matrix_free(A1);
      gsl_matrix_free(A2);
      gsl_matrix_free(R1);
      gsl_vector_free(R2);
   }

   // close files
   fclose(timings_file);
   
   return 0;
}

