CC = clang
CFLAGS = -Wall -O1 -std=gnu11
CFLAGS += $(shell gsl-config --cflags)
LDLIBS += $(shell gsl-config --libs)

all: threebody.png particle1.png particle2.png particle3.png 
	@echo
	@echo '==========================='
	@echo '=====     TASK C      ====='
	@echo '==========================='
	@echo 'Check .png plots!'
	@echo


threebody.png: threebody.ppl
	pyxplot $< 1> /dev/null 

threebody.ppl: threebody.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "threebody.png"'					>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@           		
	@echo 'set yrange [-1.2:1.2]'							>> $@   
	@echo 'set xlabel "time / arb. units"'				>> $@      		
	@echo 'set ylabel "coordinate / arb. units"'		>> $@     		
	@echo 'set title "Periodic three-body problem"'	>> $@	
	@echo 'plot \\'																						>> $@
	@echo ' "threebody.txt" using 1:2 w l lw 1 lt 1 color black title "$$x_1$$" 	\\'>> $@
	@echo ',"threebody.txt" using 1:3 w l lw 1 lt 5 color black title "$$y_1$$" 	\\'>> $@
	@echo ',"threebody.txt" using 1:4 w l lw 1 lt 1 color red   title "$$x_2$$" 	\\'>> $@
	@echo ',"threebody.txt" using 1:5 w l lw 1 lt 5 color red   title "$$y_2$$" 	\\'>> $@
	@echo ',"threebody.txt" using 1:6 w l lw 1 lt 1 color blue  title "$$x_3$$" 	\\'>> $@
	@echo ',"threebody.txt" using 1:7 w l lw 1 lt 5 color blue  title "$$y_4$$" 	\\'>> $@

particle1.png: particle1.ppl
	pyxplot $< 1> /dev/null 

particle1.ppl: threebody.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "particle1.png"'					>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@           		
	@echo 'set size ratio 0.42'							>> $@           		
	@echo 'set xrange [-1.2:1.2]'							>> $@           		
	@echo 'set yrange [-0.5:0.5]'							>> $@   
	@echo 'set mytics -0.5,0.1,0.5'						>> $@ 
	@echo 'set xlabel "$$x_1$$"'							>> $@      		
	@echo 'set ylabel "$$y_1$$"'							>> $@     		
	@echo 'set title "Periodic three-body problem (particle 1)"'								>> $@	
	@echo 'plot \\'																							>> $@
	@echo ' "threebody.txt" using 2:3 w l lw 1 lt 1 color black title "Trajectory" 	\\'>> $@

particle2.png: particle2.ppl
	pyxplot $< 1> /dev/null 

particle2.ppl: threebody.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "particle2.png"'					>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@       
	@echo 'set size ratio 0.42'							>> $@           		
	@echo 'set xrange [-1.2:1.2]'							>> $@           		
	@echo 'set yrange [-0.5:0.5]'							>> $@   
	@echo 'set mytics -0.5,0.1,0.5'						>> $@ 
	@echo 'set xlabel "$$x_2$$"'							>> $@      		
	@echo 'set ylabel "$$y_2$$"'							>> $@     		
	@echo 'set title "Periodic three-body problem (particle 2)"'								>> $@	
	@echo 'plot \\'																							>> $@
	@echo ' "threebody.txt" using 4:5 w l lw 1 lt 1 color black title "Trajectory" 	\\'>> $@

particle3.png: particle3.ppl
	pyxplot $< 1> /dev/null 

particle3.ppl: threebody.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "particle3.png"'					>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@    
	@echo 'set size ratio 0.42'							>> $@           		
	@echo 'set xrange [-1.2:1.2]'							>> $@           		
	@echo 'set yrange [-0.5:0.5]'							>> $@   
	@echo 'set mytics -0.5,0.1,0.5'						>> $@ 
	@echo 'set xlabel "$$x_3$$"'							>> $@      		
	@echo 'set ylabel "$$y_3$$"'							>> $@     		
	@echo 'set title "Periodic three-body problem (particle 3)"'								>> $@	
	@echo 'plot \\'																							>> $@
	@echo ' "threebody.txt" using 6:7 w l lw 1 lt 1 color black title "Trajectory" 	\\'>> $@

%.txt: main
	./$<

main: main.o ../AB/rungekutta.o ../util.o

clean:
	$(RM) main *.o *.txt *.ppl *.pdf *.png ../util.o

test:
	@echo $(CFLAGS)
	@echo $(LDLIBS)
