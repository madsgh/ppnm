#include<stdio.h> 
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"../util.h"

// stepper function
void rkstep23(void (*f)(int n, double x, double* y, double* dydx), int n, double* yx, double h, double* yh, double* err);

// driver function
void odedriver(
	void (*f)(int n, double x, double* y, double* dydx), // right-hand-side of dy/dx = f(x, y) 
   int n,          // size of vectors 
	double  a,      // the start-point a 
	double  b,      // the end-point of the integration 
	double* ya,     // y(a) 
	double* yb,     // y(b) to be calculated 
	double  h,      // initial step-size 
	double  acc,    // absolute accuracy goal 
	double  eps,    // relative accuracy goal 
	char*   outfile // trajectory file
);

//
// TEST: gravitational three-body problem (2D case, masses = 1)
//

// F = ma = a                 (because m = 1)
// F12 = 1/R12**2             (in appropriate units)
//     = 1/(∆x**2 + ∆y**2)
// F12_vec = n12_vec * F12    (n12_vec is a unit vector pointing from 1 to 2)
// n12_vec = (∆x, ∆y)/(∆x**2 + ∆y**2)**(1/2)
// F12_vec = (∆x, ∆y)/(∆x**2 + ∆y**2)**(3/2)
//         = (∆x, ∆y)*(∆x**2 + ∆y**2)**(-3/2)

// 00: [ r_0x' ]   [ v_0x ]   [ v_0x          ]   [ y_06 ]
// 01: [ r_0y' ]   [ v_0y ]   [ v_0y          ]   [ y_07 ]
// 02: [ r_1x' ]   [ v_1x ]   [ v_1x          ]   [ y_08 ]
// 03: [ r_1y' ]   [ v_1y ]   [ v_1y          ]   [ y_09 ]
// 04: [ r_2x' ]   [ v_2x ]   [ v_2x          ]   [ y_10 ]
// 05: [ r_2y' ] = [ v_2y ] = [ v_2y          ] = [ y_11 ]
// 06: [ v_0x' ]   [ a_0x ]   [ F_01x + F_02x ]   [ (y_02 - y_00)*((y_02 - y_00)**2 + (y_03 - y_01)**2)**(-3/2) + (2 -> 4, 3 -> 5) ]
// 07: [ v_0y' ]   [ a_0y ]   [ F_01y + F_02y ]   [ (y_03 - y_01)*((y_02 - y_00)**2 + (y_03 - y_01)**2)**(-3/2) + (2 -> 4, 3 -> 5) ]  
// 08: [ v_1x' ]   [ a_1x ]   [ F_10x + F_12x ]   [ (y_00 - y_02)*((y_00 - y_02)**2 + (y_01 - y_03)**2)**(-3/2) + (0 -> 4, 1 -> 5) ]
// 09: [ v_1y' ]   [ a_1y ]   [ F_10y + F_12y ]   [ (y_01 - y_03)*((y_00 - y_02)**2 + (y_01 - y_03)**2)**(-3/2) + (0 -> 4, 1 -> 5) ]
// 10: [ v_2x' ]   [ a_2x ]   [ F_20x + F_21x ]   [ (y_00 - y_04)*((y_00 - y_04)**2 + (y_01 - y_05)**2)**(-3/2) + (0 -> 2, 1 -> 3) ]
// 11: [ v_2y' ]   [ a_2y ]   [ F_20y + F_21y ]   [ (y_00 - y_05)*((y_00 - y_04)**2 + (y_01 - y_05)**2)**(-3/2) + (0 -> 2, 1 -> 3) ]




// RHS function for gravitational three-body problem
void f(int n, double x, double* y, double*dydx) {
   dydx[0] = y[6];
   dydx[1] = y[7];
   dydx[2] = y[8];
   dydx[3] = y[9];
   dydx[4] = y[10];
   dydx[5] = y[11];

   double R01 = pow(y[2] - y[0], 2) + pow(y[3] - y[1], 2); // actually R**2
   double R02 = pow(y[4] - y[0], 2) + pow(y[5] - y[1], 2); // .
   double R12 = pow(y[4] - y[2], 2) + pow(y[5] - y[3], 2); // .

   dydx[6]  = (y[2] - y[0])*pow(R01, -3.0/2) + (y[4] - y[0])*pow(R02, -3.0/2); // F_01x + F_02x
   dydx[7]  = (y[3] - y[1])*pow(R01, -3.0/2) + (y[5] - y[1])*pow(R02, -3.0/2); // F_01y + F_02y
   dydx[8]  = (y[0] - y[2])*pow(R01, -3.0/2) + (y[4] - y[2])*pow(R12, -3.0/2); // F_10x + F_12x
   dydx[9]  = (y[1] - y[3])*pow(R01, -3.0/2) + (y[5] - y[3])*pow(R12, -3.0/2); // F_10y + F_12y
   dydx[10] = (y[0] - y[4])*pow(R02, -3.0/2) + (y[2] - y[4])*pow(R12, -3.0/2); // F_20x + F_21x
   dydx[11] = (y[1] - y[5])*pow(R02, -3.0/2) + (y[3] - y[5])*pow(R12, -3.0/2); // F_20y + F_21y

}

int main() {
   

   // declare and set variables for odedriver
   int n = 12; 
   double a = 0;
   double b = 10;
   double ya[n];
   double yb[n];
   double h = 0.001;
   double acc = 1e-6;
   double eps = 1e-6;
   char* outfile = "threebody.txt";

   // set initial values
   ya[0]  =  0.97000436;
   ya[1]  = -0.24308753;
   ya[2]  = -0.97000436;
   ya[3]  =  0.24308753;
   ya[4]  =  0;
   ya[5]  =  0;

   ya[6]  =  0.93240737/2;
   ya[7]  =  0.86473146/2;
   ya[8]  =  0.93240737/2;
   ya[9]  =  0.86473146/2;
   ya[10] = -0.93240737;
   ya[11] = -0.86473146;

   // call driver
   odedriver(&f, n, a, b, ya, yb, h, acc, eps, outfile);


   return 0;
}
