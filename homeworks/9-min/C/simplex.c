#include<stdio.h>
#include<math.h>
#include<float.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// simplex update
void simplex_update
   (  double** simplex
   ,  double*  fxs
   ,  int      d
   ,  int*     hi
   ,  int*     lo
   ,  double*  centroid
   )
{
   // reset indices for highest and lowest points
   *hi = 0;
   *lo = 0;
   double fhi = fxs[0];
   double flo = fxs[0];

   // find highest and lowest points
   double fx;
   for (int i = 1; i < d + 1; ++i) {
      fx = fxs[i];
      if (fx > fhi) {fhi = fx; *hi = i;}
      if (fx > flo) {flo = fx; *lo = i;}
   }

   // calculate centroid
   for (int i = 0; i < d; ++i) {  
      double sum = 0;
      for (int j = 0; j < d + 1; ++j) if (j != *hi) sum += simplex[j][i];
      centroid[i] = sum/d;
   }
}


// initiate downhill simplex algo
void simplex_initiate
   (  double   func(double* x)
   ,  double** simplex
   ,  double*  fxs
   ,  int      d
   ,  int*     hi
   ,  int*     lo
   ,  double*  centroid
   )
{
   // calculate function value at vertices
   for (int i = 0; i < d + 1; ++i) fxs[i] = func(simplex[i]);

   // call updator
   simplex_update(simplex, fxs, d, hi, lo, centroid);
}


// reflection
void reflection(double* fhi, double* centroid, int d, double* reflected) {
   for (int i = 0; i < d; ++i) reflected[i] = 2*centroid[i] - fhi[i];
}

// expansion
void expansion(double* fhi, double* centroid, int d, double* expanded) {
   for (int i = 0; i < d; ++i) expanded[i] = 3*centroid[i] - 2*fhi[i];
}

// contraction
void contraction(double* fhi, double* centroid, int d, double* contracted) {
   for (int i = 0; i < d; ++i) contracted[i] = 0.5*centroid[i] + 0.5*fhi[i];
}

// reduction
void reduction(double** simplex, int d, int lo) {
   for (int j = 0; j < d + 1; ++j) 
      if (j != lo)
         for (int i = 0; i < d; ++i) 
            simplex[j][i] = 0.5*(simplex[j][i] + simplex[lo][i]);
}

// distance between two points
double distance(double* a, double* b, int d) {
   double sum = 0;
   for (int i = 0; i < d; ++i) sum += pow(b[i] - a[i], 2);
   return sqrt(sum);
}

// size of simplex
double size(double** simplex, int d) { 
   double result = 0;
   double dist;
   for (int i = 0; i < d + 1; ++i) {
      dist = distance(simplex[0], simplex[i], d);
      if (dist > result) result = dist;
   }
   return result;
}

// simplex driver
int simplex_driver
   (  double   func(double* x)
   ,  double** simplex
   ,  int      d
   ,  double   sizegoal
   )
{
   // initialize variables
   int hi = 0;
   int lo = 0;
   int k  = 0;
   double centroid[d];
   double fxs[d + 1];
   double p1[d];
   double p2[d];

   // initiate simplex
   simplex_initiate(func, simplex, fxs, d, &hi, &lo, centroid);

   // loop until convergence
   while (size(simplex, d) > sizegoal) {

      // update stats and try reflection
      simplex_update(simplex, fxs, d, &hi, &lo, centroid);
      reflection(simplex[hi], centroid, d, p1);
      double f_re = func(p1);

      // if reflection is good, try expansion
      if (f_re < fxs[lo]) {
         expansion(simplex[hi], centroid, d, p2);
         double f_ex = func(p2);

         // accept expansion?
         if (f_ex < f_re) { 
            for (int i = 0; i < d; ++i) simplex[hi][i] = p2[i]; fxs[hi] = f_ex;
         }

         // accept reflection
         else { 
            for (int i = 0; i < d; ++i) simplex[hi][i] = p1[i]; fxs[hi] = f_re;
         }
      }

      // if reflection is not good, do this instead
      else {

         // accept reflection if it is tolerable
         if (f_re < fxs[hi]) {
            for (int i = 0; i < d; ++i) simplex[hi][i] = p1[i]; fxs[hi] = f_re;
         } 
         
         // else try contraction
         else {
            contraction(simplex[hi], centroid, d, p1);
            double f_co = func(p1);

            // accept contraction?
            if (f_co < fxs[hi]) {
               for (int i = 0; i < d; ++i) simplex[hi][i] = p1[i]; fxs[hi] = f_re;
            }

            // do reduction as last resort
            else {
               reduction(simplex, d, lo); 
               simplex_initiate(func, simplex, fxs, d, &hi, &lo, centroid);
            }
         }

      } /* end else */

      // increment counter
      k++;

   } /* end while */

   // return number of iterations
   return k;

}
