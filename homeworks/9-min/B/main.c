#include<stdio.h>
#include<math.h>
#include<float.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// quasi-newton solver
int qnewton(double (*f)(gsl_vector* x), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, int verb, char* logname);

// breit-wigner function
double bw(double E, gsl_vector* param) {
   double m     = gsl_vector_get(param, 0);
   double gamma = gsl_vector_get(param, 1);
   double A     = gsl_vector_get(param, 2);
   return A/(pow(E - m, 2) + gamma*gamma/4);
}

// error function
gsl_vector* E;
gsl_vector* sigma;
gsl_vector* dsigma;
int N;
double dev(gsl_vector* param) {
   double sum = 0;
   double E_i, sigma_i, dsigma_i;
   for (int i = 0; i < N; ++i) {
      E_i      = gsl_vector_get(E, i);
      sigma_i  = gsl_vector_get(sigma, i);
      dsigma_i = gsl_vector_get(dsigma, i);
      sum     += pow(bw(E_i, param) - sigma_i, 2)/pow(dsigma_i, 2);
   }
   return sum;
}


// do stuff
int main() {
  
   // read in data
   N = 30;
   E      = gsl_vector_alloc(N);
   sigma  = gsl_vector_alloc(N);
   dsigma = gsl_vector_alloc(N);
   FILE* E_file       = fopen("energy.txt", "r");
   FILE* sigma_file   = fopen("sigma.txt", "r");
   FILE* dsigma_file  = fopen("dsigma.txt", "r");
   gsl_vector_fscanf(E_file, E);
   gsl_vector_fscanf(sigma_file, sigma);
   gsl_vector_fscanf(dsigma_file, dsigma);
   fclose(E_file);
   fclose(sigma_file);
   fclose(dsigma_file);
   
   // set up problem
   printf("Trying to fit Breit-Wigner function to data\n\n");
   int n = 3;
   gsl_vector* result = gsl_vector_alloc(n); 
   gsl_vector* x0     = gsl_vector_alloc(n); 
   gsl_vector_set(x0, 0, 125); // m
   gsl_vector_set(x0, 1, 5); // gamma
   gsl_vector_set(x0, 2, 5); // A
   char logname[] = "breitwigner.txt";
   double eps  = 1e-6;
   int maxiter = 200;
   int verbose = 1;
   double h    = 1.0*sqrt(DBL_EPSILON);
   printf("Step-size for finite differences: h = %g\n", h);
  
   // call newton solver 
   int converged = qnewton(dev, x0, eps, h, maxiter, result, verbose, logname);
   if (converged == 1) {
      printf("\nOptimization converged! Found minimum at (m, gamma, A) = \n");
      vector_print(result);
   } else {
      printf("\nOptimization not converged! Please investigate!\n");
   }
  
   // free memory
   gsl_vector_free(x0);
   gsl_vector_free(result);

   return 0;
}
