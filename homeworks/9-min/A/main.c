#include<stdio.h>
#include<math.h>
#include<float.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// quasi-newton solver
int qnewton(double (*f)(gsl_vector* x), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, int verb, char* logname);

// rosenbrock function
double f(gsl_vector* vec) {
   double x = gsl_vector_get(vec, 0);
   double y = gsl_vector_get(vec, 1);
   return pow(1 - x, 2) + 100*pow(y - x*x, 2);
}

// himmelblau function
double g(gsl_vector* vec) {
   double x = gsl_vector_get(vec, 0);
   double y = gsl_vector_get(vec, 1);
   return pow(x*x + y - 11, 2) + pow(x + y*y - 7, 2);
}


// do stuff
int main() {
  
   // ROSENBROCK
   { 
   // set up problem 
   printf("------------------------------------------------------------------------------------\n");
   printf("Searching for minimum of Rosenbrock function f(x,y) = (1 - x)**2 + 100*(y - x**2)**2\n");
   int n = 2;
   gsl_vector* result = gsl_vector_alloc(n); 
   gsl_vector* x0     = gsl_vector_alloc(n); 
   gsl_vector_set(x0, 0, 2);
   gsl_vector_set(x0, 1, 2);
   char logname[] = "rosenbrock.txt";
   double eps  = 1e-6;
   int maxiter = 200;
   int verbose = 1;
   double h    = 1.0*sqrt(DBL_EPSILON);
   printf("Step-size for finite differences: h = %g\n", h);
   
   // call newton solver 
   int converged = qnewton(f, x0, eps, h, maxiter, result, verbose, logname);
   if (converged == 1) {
      printf("\nOptimization converged! Found minimum at (x,y) = \n");
      vector_print(result);
   } else {
      printf("\nOptimization not converged! Please investigate!\n");
   }
  
   // free memory
   gsl_vector_free(x0);
   gsl_vector_free(result);
   }


   // HIMMELBLAU
   { 
   // set up problem 
   printf("------------------------------------------------------------------------------------------\n");
   printf("Searching for minimum of Himmelblau function g(x,y) = (x*x + y - 11)**2 + (x + y*y - 7)**2\n");
   int n = 2;
   gsl_vector* result = gsl_vector_alloc(n); 
   gsl_vector* x0     = gsl_vector_alloc(n); 
   gsl_vector_set(x0, 0, 2);
   gsl_vector_set(x0, 1, 2);
   char logname[] = "himmelblau.txt";
   double eps  = 1e-6;
   int maxiter = 200;
   int verbose = 1;
   double h    = 1.0*sqrt(DBL_EPSILON);
   printf("Step-size for finite differences: h = %g\n", h);
   
   // call newton solver 
   int converged = qnewton(g, x0, eps, h, maxiter, result, verbose, logname);
   if (converged == 1) {
      printf("\nOptimization converged! Found minimum at (x,y) = \n");
      vector_print(result);
   } else {
      printf("\nOptimization not converged! Please investigate!\n");
   }
  
   // free memory
   gsl_vector_free(x0);
   gsl_vector_free(result);
   }

   

   return 0;
}
