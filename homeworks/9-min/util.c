#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"util.h"

//
// Utilities
//

// print gsl vector
void vector_print(gsl_vector* vec){

	for(int i = 0; i < vec->size; i++) {
      printf("% 10.4e\n", gsl_vector_get(vec,i));
   }
	printf("\n");
}

// print gsl matrix
void matrix_print(gsl_matrix* mat){

	for(int i = 0; i < mat->size1; i++) {
	   for(int j = 0; j < mat->size2; j++) {
         printf("% 10.4e ", gsl_matrix_get(mat, i, j));
      }
	   printf("\n");
   }
	printf("\n");
}



// print gsl vector to file
void vector_fprint(FILE* file, gsl_vector* vec){

	for(int i = 0; i < vec->size; i++) {
      fprintf(file, "% 10.4e\n", gsl_vector_get(vec,i));
   }
	fprintf(file, "\n");
}

// print gsl matrix to file
void matrix_fprint(FILE* file, gsl_matrix* mat){

	for(int i = 0; i < mat->size1; i++) {
	   for(int j = 0; j < mat->size2; j++) {
         fprintf(file, "% 10.4e ", gsl_matrix_get(mat, i, j));
      }
	   fprintf(file, "\n");
   }
	fprintf(file, "\n");
}


// print c vector
void cvector_print(double* vec, int n){

	for(int i = 0; i < n; i++) {
      printf("%10.4f\n", vec[i]);
   }
	printf("\n");
}


// binary search for gsl_vector
int binsearch_vector(gsl_vector* x, double x_new) { 
   int N = x->size;
	assert(gsl_vector_get(x, 0) <= x_new && x_new <= gsl_vector_get(x, N-1));
	int i = 0;
   int j = N-1;

	while (j-i > 1) {
		int mid = (i + j)/2;
		if (x_new > gsl_vector_get(x, mid)) {
         i = mid;
      } else {
         j = mid;
      }
   }

	return i;
}

// binary search for plain c array
int binsearch_array(int N, double* x, double x_new) { 
	assert(x[0] <= x_new && x_new <= x[N-1]);
	int i = 0;
   int j = N-1;

	while (j-i > 1) {
		int mid = (i + j)/2;
		if (x_new > x[mid]) {
         i = mid;
      } else {
         j = mid;
      }
   }

	return i;
}
