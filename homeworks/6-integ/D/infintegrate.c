#include<stdio.h>
#include<math.h>
#include<assert.h>

// adaptive, recursive quadrature (open with four points)
double qquad(double f(double g(double), double, double, double), double g(double), double a0, double b0, 
             double a, double b, double f2, double f3, double abs, double rel, int nrecur, double* error) {
   assert(nrecur < 1e5);
   double f1 = f(g, a + 1*(b-a)/6, a0, b0);
   double f4 = f(g, a + 5*(b-a)/6, a0, b0);
   double Q = (b-a)*(2*f1 + f2 + f3 + 2*f4)/6;
   double q = (b-a)*(  f1 + f2 + f3 +   f4)/4;
   double err = fabs(Q-q);
   if (err < abs + rel*fabs(Q)) {
      *error = err;
      return Q;
   } else {
      return qquad(f, g, a0, b0, a, (a+b)/2, f1, f2, abs/sqrt(2), rel, nrecur + 1, error) 
           + qquad(f, g, a0, b0, (a+b)/2, b, f3, f4, abs/sqrt(2), rel, nrecur + 1, error);
   }
}

// struct for holding integration result and number of calls
typedef struct {
   double value;
   double error;
   int ncalls;
} integ;

// avoid nested functions (this is ugly but should always work)
int ncalls;
double ftrans_a(double f(double), double x, double a, double b) { ncalls++; return (f((1 - x)/x) + f(-(1 - x)/x))/(x*x); }
double ftrans_b(double f(double), double x, double a, double b) { ncalls++; return f(a + (1 - x)/x)/(x*x); }
double ftrans_c(double f(double), double x, double a, double b) { ncalls++; return f(b - (1 - x)/x)/(x*x); }
double ftrans_d(double f(double), double x, double a, double b) { ncalls++; return f(x); }

// integration wrapper that accepts infinite limits via variable substitution (uses gcc nested functions!)
void infintegrate(double f(double), double a, double b, double abs, double rel, integ* result) {
  
   assert(a < b && "Integration limits must be increasing, a < b!");

   // initialize variables
   int nrecur = 0;
   double value, error, A, B, f2, f3;

   // do variable tranformation if necessary 
   if (isinf(a) && isinf(b)) {

      A = 0; B = 1;   
      f2 = ftrans_a(f, A + 2.0*(B-A)/6.0, a, b);
      f3 = ftrans_a(f, A + 4.0*(B-A)/6.0, a, b);
      value = qquad(ftrans_a, f, a, b, A, B, f2, f3, abs, rel, nrecur, &error);

   } else if (isinf(b)) {

      A = 0; B = 1;   
      f2 = ftrans_b(f, A + 2.0*(B-A)/6.0, a, b);
      f3 = ftrans_b(f, A + 4.0*(B-A)/6.0, a, b);
      value = qquad(ftrans_b, f, a, b, A, B, f2, f3, abs, rel, nrecur, &error);

   } else if (isinf(a)) {

      A = 0; B = 1;   
      f2 = ftrans_c(f, A + 2.0*(B-A)/6.0, a, b);
      f3 = ftrans_c(f, A + 4.0*(B-A)/6.0, a, b);
      value = qquad(ftrans_c, f, a, b, A, B, f2, f3, abs, rel, nrecur, &error);

   } else { 

      A = a; B = b;   
      f2 = ftrans_d(f, A + 2.0*(B-A)/6.0, a, b);
      f3 = ftrans_d(f, A + 4.0*(B-A)/6.0, a, b);
      value = qquad(ftrans_d, f, a, b, A, B, f2, f3, abs, rel, nrecur, &error);

   }

   // write data to result struct
   result->value = value; 
   result->error = error; 
   result->ncalls = ncalls;
   ncalls = 0;

}



