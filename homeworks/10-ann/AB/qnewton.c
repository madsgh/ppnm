#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// wrapper for calculating norm
double norm(gsl_vector* x) {
   return gsl_blas_dnrm2(x);
}

// calculate gradient vector at x0 with Gi = df/dxi
void gradient(double (*f)(gsl_vector* x), gsl_vector* x0, gsl_vector* G, double h) {

   // get size of problem and f(x0) = y0
   int n = x0->size;
   double y0 = f(x0);

   // loop over entries in gradient
   gsl_vector* x1 = gsl_vector_alloc(n);
   double y1;
   for (int j = 0; j < n; ++j) {
      gsl_vector_set_basis(x1, j);        // x1                    = (0, ..., 1, ..., 0) (non-zero in the jth entry)
      gsl_vector_axpby(1, x0, h, x1);     // x1 <- x0 + h*x1, h*x1 = (0, ..., h, ..., 0) (non-zero in the jth entry)
      y1 = f(x1);                         // y1 <- f(x1)
      gsl_vector_set(G, j, (y1 - y0)/h);
   }

   // free vectors
   gsl_vector_free(x1);

}


// find root using newton's methods
int qnewton(double (*f)(gsl_vector* x), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, int verb, char* logname) {
   
   // set counter
   int iter = 0;

   // get size of vectors and allocate
   int n = x0->size;
   FILE* log = fopen(logname, "w");
   gsl_matrix* B    = gsl_matrix_alloc(n, n);
   gsl_vector* G    = gsl_vector_alloc(n);
   gsl_vector* Gnew = gsl_vector_alloc(n);
   gsl_vector* xnew = gsl_vector_alloc(n);
   gsl_vector* dx   = gsl_vector_alloc(n);
   gsl_vector* y    = gsl_vector_alloc(n);
   gsl_vector* u    = gsl_vector_alloc(n);
   gsl_matrix_set_identity(B);
   double y0, ynew, dxtG, uty;
   int converged = 0;
   int step_too_small = 0;
   int reached_max_iter = 0;

   // calculate inital function value and gradient
   y0 = f(x0);
   gradient(f, x0, G, h);

   // loop until convergence
   while (1) {

      // check that we are not exceeding max number of iterations
      iter++;
      if (iter > maxiter) {
         reached_max_iter = 1;
         break;
      }

      // print function at x0 
      if (verb) fprintf(log, "\n=========== Iteration %d ===========\n", iter);
      if (verb) fprintf(log, "x0 = \n");
      if (verb) vector_fprint(log, x0);
      if (verb) fprintf(log, "y0 = % .4e\n", y0);

      // print gradient
      if (verb) fprintf(log, "grad =\n");
      if (verb) vector_fprint(log, G);
      if (verb) fprintf(log, "||grad|| = % .4e\n", norm(G));
      if (norm(G) < eps) {
         converged = 1;
         break;
      }

      // calculate newton step (dx = -B*grad)
      gsl_blas_dgemv(CblasNoTrans, -1, B, G, 0, dx); // dx <- -B*G
      if (verb) fprintf(log, "inverse hessian B =\n");
      if (verb) matrix_fprint(log, B);
      if (verb) fprintf(log, "newton step dx = -B*grad\n");
      if (verb) vector_fprint(log, dx);
      if (norm(dx) < h*norm(x0)) {
         step_too_small = 1;
         break;
      }

      // do simple backtracking line search
      if (verb) fprintf(log, "****** Starting line search ******\n");
      double lambda = 1.0;
      while (1) {

         gsl_vector_memcpy(xnew, x0);
         gsl_vector_add(xnew, dx);
         ynew = f(xnew);                     
         gsl_blas_ddot(dx, G, &dxtG);
         if (verb) fprintf(log, "xnew = x0 + %.4e*dx = \n", lambda);
         if (verb) vector_fprint(log, xnew);
         if (verb) fprintf(log, "ynew           = % .4e\n", ynew);
         if (verb) fprintf(log, "y0 + 0.01*dxtG = % .4e\n\n", y0 + 0.01*dxtG);
         
         // break if armijo condition is satisfied
         if (ynew < y0 + 0.01*dxtG) { 
            if (verb) fprintf(log, "Armijo condition satified!\n");
            break;
         }

         // break if step gets too small
         if (lambda < h) { 
            if (verb) fprintf(log, "Line-search step too small! Inverse Hessian reset to identity!\n");
            gsl_matrix_set_identity(B);
            break;
         }

         lambda *= 0.5;
         gsl_vector_scale(dx, 0.5);

      };
      if (verb) fprintf(log, "******   Line search done   ******\n");


   // calculate new gradient and update inverse hessian (SR1 update)
   gradient(f, xnew, Gnew, h);     // calculate new gradient

   gsl_vector_memcpy(y, Gnew);  // ...
   gsl_blas_daxpy(-1, G, y);    // y = Gnew - G 

   gsl_vector_memcpy(u, dx);                 // ...
   gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u);  // u = dx - B*y

   gsl_blas_ddot(u, y, &uty);   // calculate ut*y
   if (verb) fprintf(log, "\nut*y = % .4e\n", uty);
   if (fabs(uty) > 1e-12) { 
      gsl_blas_dger(1.0/uty, u, u, B); // B += u*ut/(ut*y)
      if (verb) fprintf(log, "I have updated B!\n");
   }
   else if (verb) fprintf(log, "Did not update B!\n");
   
   
   // make the step
   gsl_vector_memcpy(x0, xnew);
   gsl_vector_memcpy(G, Gnew);
   y0 = f(x0);


   }; 

   // when converged do this 
   gsl_vector_memcpy(result, x0);   // copy result into result vector
   f(result);                       // call target function on result (useful if the target function makes a plot)
   if (verb) fprintf(log, "\n-----------------------------------------------\n");
   if (verb) fprintf(log, "Quasi-Newton solver ended after %3d iterations!\n", iter);
   if (verb) fprintf(log, "-----------------------------------------------\n\n");

   if (verb && converged == 1)        fprintf(log, "Optimization converged!\n");
   if (verb && step_too_small == 1)   fprintf(log, "Optimization not converged! Newton step too small!\n");
   if (verb && reached_max_iter == 1) fprintf(log, "Optimization not converged! Too many iterations!\n");

   // free memory
   gsl_matrix_free(B);
   gsl_vector_free(dx);
   gsl_vector_free(xnew);
   gsl_vector_free(y);
   gsl_vector_free(u);
   gsl_vector_free(G);
   gsl_vector_free(Gnew);
   fclose(log);

   // return convergence status
   return converged;
   
}
