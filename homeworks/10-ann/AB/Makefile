CC = gcc-10
CFLAGS = -Wall -O1 -std=gnu11
CFLAGS += $(shell gsl-config --cflags)
LDLIBS += $(shell gsl-config --libs)

all: output.txt plot.png
	@echo
	@echo '===================================='
	@echo '========    TASK A and B    ========'
	@echo '===================================='
	@echo
	@echo 'Check output below and plot.png. Verbose output from minimization' 
	@echo 'can be found in training.txt'
	@cat  output.txt 
	@echo 

plot.png: plot.ppl
	pyxplot $< 1> /dev/null 

plot.ppl: xytrain.txt Makefile
	@echo 'set terminal png dpi 600'						>  $@ 
	@echo 'set output "plot.png"'							>> $@           		
	@echo 'set key outside' 								>> $@           		
	@echo 'set grid'											>> $@           		
	@echo 'set xlabel "$$x$$"'								>> $@      		
	@echo 'set ylabel "$$y$$"'								>> $@     		
	@echo 'set title "Artificial neural network"'	>> $@	
	@echo 'plot \\'											>> $@
	@echo ' "fit.txt" using 1:2 w l lt 1 color black  title "Curve fit" 											\\'>> $@
	@echo ',"fit.txt" using 1:3 w l lt 2 color black  title "Derivative of fit" 								\\'>> $@
	@echo ',"fit.txt" using 1:4 w l lt 3 color black  title "Antiderivative of fit" 							\\'>> $@
	@echo ',"xytrain.txt" using 1:2 w p ps 0.7 color blue  title "Training function" 						\\'>> $@
	@echo ',"xytrain.txt" using 1:3 w p ps 0.7 color blue  title "Derivative of training function" 		\\'>> $@
	@echo ',"xytrain.txt" using 1:($$4 - 0.7) w p ps 0.7 color blue  title "Antiderivative of training function" \\'>> $@

xytrain.txt: xtrain.txt ytrain.txt dtrain.txt itrain.txt
	@paste xtrain.txt ytrain.txt dtrain.txt itrain.txt > xytrain.txt

output.txt xtrain.txt ytrain.txt: main
	./$< > $@

main: main.o ann.o qnewton.o ../util.o

clean:
	$(RM) main *.o *.txt *.ppl *.pdf *.png ../util.o

test:
	@echo $(CFLAGS)
	@echo $(LDLIBS)
