#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<float.h>
#include"../util.h"


// quasi-newton solver
int qnewton(double (*f)(gsl_vector* x), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, int verb, char* logname);

// struct to store network
typedef struct { 
   int n;                    // number of hidden neurons
   double (*f)(double);      // activation function
   double (*deriv)(double);  // derivative of activation function
   double (*integ)(double);  // anti-derivative of activation function
   gsl_vector* params;       // vector to store 3*n parameters {ai, bi, wi}
} ann;

// allocate memory for ann
ann* ann_alloc (int n, double (*f)(double), double (*g)(double), double (*h)(double)) {
   ann* network = (ann*)malloc(sizeof(ann));
   network->n = n; 
   network->f = f;
   network->deriv = g;
   network->integ = h;
   network->params = gsl_vector_alloc(3*n);
   return network;
}

// free memory
void ann_free (ann* network) {
   gsl_vector_free(network->params);
   free(network);
}

// get output of ann
double ann_response (ann* network, double x) { 
   double y = 0;
   double a, b, w;
   for (int i = 0; i < network->n; ++i) {
      a = gsl_vector_get(network->params, 3*i);
      b = gsl_vector_get(network->params, 3*i + 1);
      w = gsl_vector_get(network->params, 3*i + 2);
      y += w*network->f((x - a)/b);
   }
   return y;
} 

// get derivative of ann
double ann_deriv (ann* network, double x) { 
   double y = 0;
   double a, b, w;
   for (int i = 0; i < network->n; ++i) {
      a = gsl_vector_get(network->params, 3*i);
      b = gsl_vector_get(network->params, 3*i + 1);
      w = gsl_vector_get(network->params, 3*i + 2);
      y += (w/b)*network->deriv((x - a)/b);
   }
   return y;
} 

// get anti-derivative of ann
double ann_integ (ann* network, double x) { 
   double y = 0;
   double a, b, w;
   for (int i = 0; i < network->n; ++i) {
      a = gsl_vector_get(network->params, 3*i);
      b = gsl_vector_get(network->params, 3*i + 1);
      w = gsl_vector_get(network->params, 3*i + 2);
      y += b*w*network->integ((x - a)/b);
   }
   return y;
} 



// optimize parameters according to training data
void ann_train (ann* network, gsl_vector* xs, gsl_vector* ys) {
   
   // Number of data points
   assert(xs->size == ys->size && "ann_train: xs->size must be equal to ys->size");
   int N = xs->size;

   // Set up deviation function
   double dev (gsl_vector* p) {
      gsl_vector_memcpy(network->params, p);
      double sum = 0;
      for (int i = 0; i < N; ++i) { 
         double xi = gsl_vector_get(xs, i);
         double yi = gsl_vector_get(ys, i);
         sum += pow(ann_response(network, xi) - yi, 2);
      }
      return sum;
   }

   // set up problem
   gsl_vector* params1 = gsl_vector_alloc(3*network->n); 
   gsl_vector* params0 = gsl_vector_alloc(3*network->n); 
   gsl_vector_memcpy(params0, network->params);   
   char logname[] = "training.txt";
   double eps  = 5e-2;
   int maxiter = 400;
   int verbose = 1;
   double h    = 1.0*sqrt(DBL_EPSILON);
  
   // call newton solver 
   int converged = qnewton(dev, params0, eps, h, maxiter, params1, verbose, logname);
   if (converged == 1) {
      printf("\nOptimization converged! Found minimum at (ai, bi, wi) = \n");
      vector_print(params1);
      gsl_vector_memcpy(network->params, params1);
   } else {
      printf("\nOptimization not converged! Please investigate!\n");
   }
  
   // free memory
   gsl_vector_free(params0);
   gsl_vector_free(params1);

}


