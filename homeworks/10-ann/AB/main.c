#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<math.h>
#include"../util.h"

// struct to store network
typedef struct { 
   int n;                    // number of hidden neurons
   double (*f)(double);      // activation function
   double (*deriv)(double);  // derivative of activation function
   double (*integ)(double);  // anti-derivative of activation function
   gsl_vector* params;       // vector to store 3*n parameters {ai, bi, wi}
} ann;


// ann functions
ann*   ann_alloc   (int n, double (*f)(double), double (*deriv)(double), double (*integ)(double));
void   ann_free    (ann* network);
double ann_response(ann* network, double x);
double ann_deriv   (ann* network, double x);
double ann_integ   (ann* network, double x);
void   ann_train   (ann* network, gsl_vector* xs, gsl_vector* ys);

// function to interpolate
double func (double x) { 
   return cos(5*x-1)*exp(-fabs(x));
}

// derivative of func
double func_deriv (double x) { 
   if (x <= 0) return -5*sin(5*x - 1)*exp(+x) + cos(5*x - 1)*exp(+x);
   else        return -5*sin(5*x - 1)*exp(-x) - cos(5*x - 1)*exp(-x);
}

// antiderivative of func
double func_integ (double x) { 
   if (x <= 0) return (5*sin(5*x - 1) + cos(5*x - 1))*exp(+x)/26;
   else        return (5*sin(5*x - 1) - cos(5*x - 1))*exp(-x)/26;
}


// activation function
double activation (double x) { 
   return x*exp(-x*x);
}

// derivative of activation function
double derivative (double x) { 
   return exp(-x*x) - 2*x*x*exp(-x*x);
}

// antiderivative of activation function
double integral (double x) { 
   return -exp(-x*x)/2;
}


// do stuff
int main() {

   // set up training data
   int ntrain  = 20;
   double xmin = -1;
   double xmax =  1;
   double step = (xmax - xmin)/(ntrain - 1);
   gsl_vector* xs = gsl_vector_alloc(ntrain);
   gsl_vector* ys = gsl_vector_alloc(ntrain);
   gsl_vector* ds = gsl_vector_alloc(ntrain);
   gsl_vector* is = gsl_vector_alloc(ntrain);
   for (int i = 0; i < ntrain; ++i) {
      double xi = xmin + i*step;
      gsl_vector_set(xs, i, xi); 
      gsl_vector_set(ys, i, func(xi));
      gsl_vector_set(ds, i, func_deriv(xi));
      gsl_vector_set(is, i, func_integ(xi));
   }
   FILE* xtrain = fopen("xtrain.txt", "w");
   FILE* ytrain = fopen("ytrain.txt", "w");
   FILE* dtrain = fopen("dtrain.txt", "w");
   FILE* itrain = fopen("itrain.txt", "w");
   gsl_vector_fprintf(xtrain, xs, "%e");
   gsl_vector_fprintf(ytrain, ys, "%e");
   gsl_vector_fprintf(dtrain, ds, "%e");
   gsl_vector_fprintf(itrain, is, "%e");


   // initialize network and set initial guess at parameters
   int n = 5; 
   ann* network = ann_alloc(n, &activation, &derivative, &integral);
   for (int i = 0; i < network->n; ++i) {
      gsl_vector_set(network->params, 3*i + 0, xmin + 2*(xmax - xmin)*i/(network->n - 1)); // set ai
      gsl_vector_set(network->params, 3*i + 1, 1.0); // bi <- 1
      gsl_vector_set(network->params, 3*i + 2, 1.0); // wi <- 1
   }


   // train network
   ann_train(network, xs, ys);

   // plot curve fit
   FILE* plot = fopen("fit.txt", "w");
   int nplot = 100;
   double plotstep = (xmax - xmin)/(nplot - 1);
   for (int i = 0; i < nplot; ++i) {
      double xi = xmin + i*plotstep;
      double yi = ann_response(network, xi);
      double di = ann_deriv(network, xi);
      double ii = ann_integ(network, xi);
      fprintf(plot, "% .12e % .12e % .12e % .12e\n", xi, yi, di, ii);
   }

   

   // free memory
   ann_free(network);
   fclose(xtrain);
   fclose(ytrain);
   fclose(dtrain);
   fclose(itrain);
   fclose(plot);

   

   return 0;
}
