#include<stdio.h> 
#include<math.h>
#include<assert.h>

// plain monte carlo integration
void plainmc( int dim                        // the number of dimensions
            , double f(int dim, double* x)   // the integrand
            , double* a                      // the start-point for each dimension
            , double* b                      // the end-point for each dimension
            , int N                          // the number of points to be sampled
            , double* result                 // pointer to variable where result should be written
            , double* error                  // pointer to variable where error estimate should be written
            ); 

// some weird function to integrate
double f(int dim, double* x) {
   return 1.0/(1 - cos(x[0])*cos(x[1])*cos(x[2]))/(M_PI*M_PI*M_PI);
}

// another function
double g(int dim, double* x) { 
   if (x[0]*x[0] + x[1]*x[1] + x[2]*x[2] <= 1) return 1;
   else return 0; 
}

// yet another function
double h(int dim, double* x) {
   return pow(sin(3*M_PI*x[0])*sin(4*M_PI*x[1])*sin(5*M_PI*x[2]), 2);
}



// do stuff
int main() {
  
   // INTEGRAL OF FUNCTION f
   { 
   // fill out start- and end-points
   int dim = 3;
   double a[dim];
   double b[dim];
   for (int i = 0; i < dim; ++i) {
      a[i] = 0;
      b[i] = M_PI;
   }

   // set up variables (result, error and number of points)
   double result;
   double error;
   int N = 1e6;

   // call integrator
   long double exact = 1.3932039296856768591842462603255;
   plainmc(dim, f, a, b, N, &result, &error);
   printf("(f) Integral of (1 - cos(x)*cos(y)*cos(z))**(-1) from [0,0,0] to [pi,pi,pi]:\n");
   printf("  Using # points = %.2e\n", (double)N);
   printf("  Value  (calc.) = %.15f\n", result);
   printf("  Error  (calc.) = %.15f\n", error);
   printf("  Result (exact) = %.15Lf\n", exact);
   printf("  Error  (exact) = %.15Lf\n\n", fabsl(result - exact));
   }
  

   // INTEGRAL OF FUNCTION g
   { 
   // fill out start- and end-points
   int dim = 3;
   double a[dim];
   double b[dim];
   for (int i = 0; i < dim; ++i) {
      a[i] = -1;
      b[i] =  1;
   }

   // set up variables (result, error and number of points)
   double result;
   double error;
   int N = 1e6;

   // call integrator
   double exact = 4*M_PI/3;
   plainmc(dim, g, a, b, N, &result, &error);
   printf("(g) Integral over f(x,y,z) = 1 if inside sphere of radius 1:\n");
   printf("  Using # points = %.2e\n", (double)N);
   printf("  Value  (calc.) = %.15f\n", result);
   printf("  Error  (calc.) = %.15f\n", error);
   printf("  Result (exact) = %.15f\n", exact);
   printf("  Error  (exact) = %.15f\n\n", fabs(result - exact));
   }


   // INTEGRAL OF FUNCTION h
   { 
   // fill out start- and end-points
   int dim = 3;
   double a[dim];
   double b[dim];
   for (int i = 0; i < dim; ++i) {
      a[i] = 0;
      b[i] = 1;
   }

   // set up variables (result, error and number of points)
   double result;
   double error;
   int N = 1e6;

   // call integrator
   double exact = 1.0/8;;
   plainmc(dim, h, a, b, N, &result, &error);
   printf("(h) Integral of (sin(3*pi*x)*sin(4*pi*y)*sin(5*pi*z))**2 from [0,0,0] to [1,1,1]:\n");
   printf("  Using # points = %.2e\n", (double)N);
   printf("  Value  (calc.) = %.15f\n", result);
   printf("  Error  (calc.) = %.15f\n", error);
   printf("  Result (exact) = %.15f\n", exact);
   printf("  Error  (exact) = %.15f\n\n", fabs(result - exact));
   }


   return 0;
}
