(f) Integral of (1 - cos(x)*cos(y)*cos(z))**(-1) from [0,0,0] to [pi,pi,pi]:
  Using # points = 1.00e+06
  Value  (calc.) = 1.390191294842038
  Error  (calc.) = 0.013194897765334
  Result (exact) = 1.393203929685677
  Error  (exact) = 0.003012634843639

(g) Integral over f(x,y,z) = 1 if inside sphere of radius 1:
  Using # points = 1.00e+06
  Value  (calc.) = 4.183312000000000
  Error  (calc.) = 0.003995797381081
  Result (exact) = 4.188790204786391
  Error  (exact) = 0.005478204786391

(h) Integral of (sin(3*pi*x)*sin(4*pi*y)*sin(5*pi*z))**2 from [0,0,0] to [1,1,1]:
  Using # points = 1.00e+06
  Value  (calc.) = 0.125380886930825
  Error  (calc.) = 0.000192968556238
  Result (exact) = 0.125000000000000
  Error  (exact) = 0.000380886930825

