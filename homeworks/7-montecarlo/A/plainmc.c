#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include"../util.h"
#define RANDOM ((double)rand())/RAND_MAX

// plain monte carlo integration
void plainmc( int dim                        // the number of dimensions
            , double f(int dim, double* x)   // the integrand
            , double* a                      // the start-point for each dimension
            , double* b                      // the end-point for each dimension
            , int N                          // the number of points to be sampled
            , double* result                 // pointer to variable where result should be written
            , double* error                  // pointer to variable where error estimate should be written
            ) 
{
   // calculate volume
   double V = 1; 
   for (int i = 0; i < dim; ++i) V *= b[i] - a[i];

   // sample points
   double sum = 0;
   double sum2 = 0; 
   double x[dim];
   double fx;
   for (int i = 0; i < N; ++i) {
      for(int i = 0; i < dim; ++i) x[i] = a[i] + RANDOM*(b[i] - a[i]);
      fx = f(dim, x); 
      sum += fx; 
      sum2 += fx*fx;
   }

   // calculate results
   double mean = sum/N;                      // mean
   double sigma = sqrt(sum2/N - mean*mean);  // square root of variance
   *result = mean*V;                         // write result
   *error  = sigma*V/sqrt(N);                // write error estimate
}
