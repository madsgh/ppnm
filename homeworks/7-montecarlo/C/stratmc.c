#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#define RANDOM (double)rand()/RAND_MAX

double strata
   (  int dim
   ,  double f(int dim,double*x)
   ,  double* a
   ,  double* b
   ,  double acc
   ,  double eps
   ,  int n_reuse
   ,  double mean_reuse
   )
{
   // initial number of points to sample
   int N = 16*dim;

   // calculate integration volume
   double V = 1; 
   for (int k = 0; k < dim; ++k) V *= b[k] - a[k];

   // initialize arrays/variables and set everything to zero
   int n_l[dim], n_r[dim];
   double x[dim], mean_l[dim], mean_r[dim], mean = 0;
   for (int k = 0; k < dim; ++k) {
      mean_l[k] = 0; 
      mean_r[k] = 0; 
      n_l[k] = 0; 
      n_r[k] = 0; 
   }

   // loop over N random points
   for (int i = 0; i < N; ++i) {

      // sample a random point
      for (int k = 0; k < dim; ++k)  x[k] = a[k] + RANDOM*(b[k] - a[k]);

      // calculate f(x)
      double fx = f(dim, x);

      // divide into left and right and add to means
      for (int k = 0; k < dim; ++k) {
         if (x[k] > (a[k] + b[k])/2) { n_r[k]++; mean_r[k] += fx; }
         else                        { n_l[k]++; mean_l[k] += fx; }
      }
      mean += fx;
   }

   // remember to divide by number of points to get proper mean
   mean /= N;
   for (int k = 0; k < dim; ++k) { mean_l[k] /= n_l[k]; mean_r[k] /= n_r[k]; }

   // find best dimension to subdivide along
   int kdiv = 0; 
   double maxvar = 0;
   for (int k = 0; k < dim; ++k) {
      double var = fabs(mean_r[k] - mean_l[k]);
      if (var > maxvar) { maxvar = var; kdiv = k; }
   }
   
   // calculate integral and check if error is smaller that tolerance
   double integ = (mean*N + mean_reuse*n_reuse)/(N + n_reuse)*V;
   double error = fabs(mean_reuse - mean)*V;
   double toler = acc + fabs(integ)*eps;
   if (error < toler) return integ;

   // dispactch recursive call if error is not satisfactory
   double anew[dim];
   double bnew[dim];
   for (int k = 0; k < dim; ++k) anew[k] = a[k];
   for (int k = 0; k < dim; ++k) bnew[k] = b[k];
   anew[kdiv] = (a[kdiv] + b[kdiv])/2;
   bnew[kdiv] = (a[kdiv] + b[kdiv])/2;
   double integ_l = strata(dim, f, a, bnew, acc/sqrt(2.0), eps, n_l[kdiv], mean_l[kdiv]);
   double integ_r = strata(dim, f, anew, b, acc/sqrt(2.0), eps, n_r[kdiv], mean_r[kdiv]);
   return integ_l + integ_r;
}
