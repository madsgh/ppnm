#include<stdio.h>
#include<math.h>
#define R 0.8

// stratified monte carlo integrator
double strata
   (  int dim
   ,  double f(int dim,double*x)
   ,  double* a
   ,  double* b
   ,  double acc
   ,  double eps
   ,  int n_reuse
   ,  double mean_reuse
   );

// function to integrate
int ncalls;
double f(int dim, double* p) {
   ncalls++;
	double x = p[0];
   double y = p[1];
   fprintf(stderr, "% .6e % .6e\n", x, y);
   if (x*x + y*y <= R*R) return 1;	
   else return 0;
}


// do stuff
int main() {

   // INTEGRATE f(x, y)
   {
	double a[] = {0, 0};
   double b[] = {1, 1};
   double abseps = 1e-3;
   double releps = 1e-3;
	int dim = sizeof(a)/sizeof(a[0]);
   ncalls = 0;
	double integ = strata(dim, f, a, b, abseps, releps, 0, 0);
	double exact = M_PI*R*R/4;
	printf("Stratified MC integration of f(x,y) = 1 if x*x + y*y < R**2 else 0, R = %.2f\n", R);
	printf("  Left  boundary = [%g,%g]\n", a[0], a[1]); 
	printf("  Right boundary = [%g,%g]\n", b[0], b[1]); 
   printf("  Abs. acc. goal = %.2e\n", abseps);
   printf("  Rel. acc. goal = %.2e\n", releps);
   printf("  Value  (calc.) = %.15f\n", integ);
   printf("  Error  (calc.) = %.15f\n", abseps + fabs(integ)*releps);
   printf("  Result (exact) = %.15f\n", exact);
   printf("  Error  (exact) = %.15f\n", fabs(integ - exact));
   printf("  Number of pts  = %d\n", ncalls);
   }

}
