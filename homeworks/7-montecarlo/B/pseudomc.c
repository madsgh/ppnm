#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../util.h"
#define RANDOM ((double)rand())/RAND_MAX

// van der corput sequence
double corput(int n, int base) { 
   double q = 0;
   double bk = 1.0/base;
   while (n > 0) { 
      q += (n % base)*bk;
      n  /= base;
      bk /= base;
   }
   return q;
}

// halton sequence
double halton(int type, int n, int dim, int i) {
   if (type == 0) {
      int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67};
      int maxdim = sizeof(base)/sizeof(int);
      assert(dim <= maxdim);
      return corput(n, base[i]);
   }
   else {
      int base[] = {71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163};
      int maxdim = sizeof(base)/sizeof(int);
      assert(dim <= maxdim);
      return corput(n, base[i]);
   }
}

// halton sequence (different base)
double HALTON(int n, int dim, int i) {
   int base[] = {71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163};
   int maxdim = sizeof(base)/sizeof(int);
   assert(dim <= maxdim);
   return corput(n, base[i]);
}



// plain monte carlo integration
void pseudomc( int dim                        // the number of dimensions
             , double f(int dim, double* x)   // the integrand
             , double* a                      // the start-point for each dimension
             , double* b                      // the end-point for each dimension
             , int N                          // the number of points to be sampled
             , double* result                 // pointer to variable where result should be written
             , double* error                  // pointer to variable where error estimate should be written
             ) 
{
   // calculate volume
   double V = 1; 
   for (int i = 0; i < dim; ++i) V *= b[i] - a[i];

   // initialize variables
   double sum;
   double x[dim];
   double fx;
   
   // sample points using first halton seq 
   sum = 0;
   for (int i = 0; i < N; ++i) {
      for(int j = 0; j < dim; ++j) x[j] = a[j] + halton(0, i, dim, j)*(b[j] - a[j]);
      fx = f(dim, x); 
      sum += fx; 
   }
   double mean_a = sum/N;

   // sample points again using second halton seq
   sum = 0;
   for (int i = 0; i < N; ++i) {
      for(int j = 0; j < dim; ++j) x[j] = a[j] + halton(1, i, dim, j)*(b[j] - a[j]);
      fx = f(dim, x); 
      sum += fx; 
   }
   double mean_b = sum/N;

   // calculate results
   *result = V*(mean_a + mean_b)/2;    // write result
   *error  = fabs(V*mean_a - V*mean_b);  // write error estimate
}
