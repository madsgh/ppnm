#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_interp.h>
#include"../util.h"

//
// DECLARE FUNCTIONS (from spline3.c)
//

// structure for holding data and coefficients
typedef struct {
   int N;      // number of data points
   double* x;  // arrays
   double* y;  // .
   double* b;  // .
   double* c;  // .
   double* d;  // .
} spline3;

// function for initializing the spline
spline3* spline3_init(int N, double* x, double* y);

// function to evaluate qubic spline at point x_new
double spline3_eval(spline3* s, double x_new);

// function to evaluate slope of qubic spline at point x_new
double spline3_eval_deriv(spline3* s, double x_new);

// function to evaluate integral of qubic spline up to point x_new
double spline3_eval_integral(spline3* s, double x_new);

// function to free memory
void spline3_free(spline3* s);


   
//
// MAIN FUNCTION
//

int main() {

   //
   // READ DATA FROM FILE
   //

   // set length of vectors and allocate
   int N = 10;
   gsl_vector* x = gsl_vector_alloc(N);
   gsl_vector* y = gsl_vector_alloc(N);
   
   // read data from file and check that sizes match
   FILE* x_file = fopen("x.txt", "r");
   FILE* y_file = fopen("y.txt", "r");
   gsl_vector_fscanf(x_file, x);
   gsl_vector_fscanf(y_file, y);

   // also convert data to plain array (this is for gsl interpolation)
   double xa[x->size];
   double ya[y->size];
   for (int i = 0; i < x->size; ++i) {
      xa[i] = gsl_vector_get(x, i);
      ya[i] = gsl_vector_get(y, i);
   }


   //
   // QUBIC SPLINE
   //

   // compute qubic interpolant using gsl
   gsl_interp* gsl_interp3 = gsl_interp_alloc(gsl_interp_cspline, x->size);
   gsl_interp_init(gsl_interp3, xa, ya, x->size);
   gsl_interp_accel* acc = gsl_interp_accel_alloc();
   
   // compare homemade cubic spline with gsl_interp_cspline
   spline3* s = spline3_init(N, xa, ya);   
   double x_min = gsl_vector_get(x, 0);
   double x_max = gsl_vector_get(x, x->size - 1);
   double step = 0.05;
   int n = (x_max - x_min)/step;
   FILE* spline3_file = fopen("spline3.txt", "w");
   for (int i = 0; i <= n; ++i) {
      double x_new = i*step;
      double y_new = spline3_eval(s, x_new);
      double d_new = spline3_eval_deriv(s, x_new);
      double i_new = spline3_eval_integral(s, x_new);
      double y_gsl = gsl_interp_eval(gsl_interp3, xa, ya, x_new, acc);
      double d_gsl = gsl_interp_eval_deriv(gsl_interp3, xa, ya, x_new, acc);
      double i_gsl = gsl_interp_eval_integ(gsl_interp3, xa, ya, x_min, x_new, acc);
      fprintf(spline3_file, "%10.4f %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f\n", x_new, y_gsl, y_new, d_gsl, d_new, i_gsl, i_new);
   }

   // free memory and close files
   gsl_interp_accel_free(acc);
   gsl_interp_free(gsl_interp3);
   spline3_free(s);
   fclose(spline3_file);
   fclose(x_file);
   fclose(y_file);
   return 0;
}
