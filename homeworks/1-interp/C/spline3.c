#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include"../util.h"


//
// CUBIC SPLINE
//

// structure for holding data and coefficients
typedef struct {
   int N;      // number of data points
   double* x;  // arrays
   double* y;  // .
   double* b;  // .
   double* c;  // .
   double* d;  // .
} spline3;

// function for initializing the spline
spline3* spline3_init(int N, double* x, double* y) {

   // allocate memory
   spline3* s = (spline3*)malloc(sizeof(spline3));
   s->N = N;
   s->x = (double*)malloc(N*sizeof(double));
   s->y = (double*)malloc(N*sizeof(double));
   s->b = (double*)malloc(N*sizeof(double));
   s->c = (double*)malloc((N - 1)*sizeof(double));
   s->d = (double*)malloc((N - 1)*sizeof(double));

   // read in data
   for (int i = 0; i < N; ++i) {
      s->x[i] = x[i];
      s->y[i] = y[i];
   }

   // initialize and calculate the Δx_i and p_i
   int i;
   double dx[N-1];
   double  p[N-1];
   for (i = 0; i < N - 1; ++i) {
      dx[i] = x[i+1] - x[i];
      assert(dx[i] > 0);
      p[i]  = (y[i+1] - y[i])/dx[i];
   }
   
   // set up system of equations
   double D[N];
   double B[N];
   double Q[N-1];

   D[0]   = 2; 
   D[N-1] = 2; 
   B[0]   = 3*p[0];
   B[N-1] = 3*p[N-2];
   Q[0]   = 1;

   for (i = 0; i < N - 2; ++i) {
      D[i+1] = 2*dx[i]/dx[i+1] + 2;    
      B[i+1] = 3*(p[i] + p[i+1]*dx[i]/dx[i+1]);
      Q[i+1] = dx[i]/dx[i+1];
   }

   // gauss elimination
   for (i = 1; i < N; ++i) {
      D[i] -= Q[i-1]/D[i-1];
      B[i] -= B[i-1]/D[i-1];
   }

   // back substitution
   s->b[N-1] = B[N-1]/D[N-1];

   for (i = N - 2; i >= 0; --i) {
      s->b[i] = (B[i] - Q[i]*s->b[i+1])/D[i];
   }

   for (i = 0; i < N - 1; ++i) {
      s->c[i] = (-2*s->b[i] - s->b[i+1] + 3*p[i])/dx[i];
      s->d[i] = (s->b[i] + s->b[i+1] - 2*p[i])/dx[i]/dx[i];
   }

   return s;
}

// function to evaluate qubic spline at point x_new
double spline3_eval(spline3* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   // calculate and return interpolated value
   double dx = x_new - s->x[i];
   double y_new = s->y[i] + s->b[i]*dx + s->c[i]*pow(dx, 2) + s->d[i]*pow(dx, 3);
   return y_new;
}

// function to evaluate slope of qubic spline at point x_new
double spline3_eval_deriv(spline3* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   // calculate and return derivative
   double dx = x_new - s->x[i];
   double deriv = s->b[i] + 2*s->c[i]*dx + 3*s->d[i]*pow(dx, 2);
   return deriv;

}

// function to evaluate integral of qubic spline up to point x_new
double spline3_eval_integral(spline3* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   
   // loop over all intervals up to (but not including) the last one
   double integral = 0;
   for (int j = 0; j < i; ++j) {
      double dx = s->x[j+1] - s->x[j];
      integral += s->y[j]*dx + s->b[j]*pow(dx, 2)/2 + s->c[j]*pow(dx, 3)/3 + s->d[j]*pow(dx, 4)/4;
   }

   // handle the last interval (j = i)
   double dx = x_new - s->x[i];
   integral += s->y[i]*dx + s->b[i]*pow(dx, 2)/2 + s->c[i]*pow(dx, 3)/3 + s->d[i]*pow(dx, 4)/4;
   return integral;

}

// function to free memory
void spline3_free(spline3* s) {
   free(s->x);
   free(s->y);
   free(s->b);
   free(s->c);
   free(s->d);
   free(s);
}


