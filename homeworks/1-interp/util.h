//#include<gsl/gsl_vector>

// print gsl vector
void vector_print(gsl_vector* vec);

// binary search for gsl_vector
int binsearch_vector(gsl_vector* x, double x_new); 

// binary search for plain c array
int binsearch_array(int N, double* x, double x_new); 
