#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include"../util.h"


//
// QUADRATIC SPLINE
//

// structure for holding data and coefficients
typedef struct {
   int N;      // number of data points
   double* x;  // arrays
   double* y;  // .
   double* b;  // .
   double* c;  // .
} spline2;

// function for initializing the spline
spline2* spline2_init(int N, double* x, double* y) {

   // allocate memory
   spline2* spline = (spline2*)malloc(sizeof(spline2));
   spline->N = N;
   spline->x = (double*)malloc(N*sizeof(double));
   spline->y = (double*)malloc(N*sizeof(double));
   spline->b = (double*)malloc((N - 1)*sizeof(double));
   spline->c = (double*)malloc((N - 1)*sizeof(double));

   // read in data
   for (int i = 0; i < N; ++i) {
      spline->x[i] = x[i];
      spline->y[i] = y[i];
   }

   // initialize and calculate the Δx_i and p_i
   int i;
   double dx[N-1];
   double  p[N-1];
   for (i = 0; i < N - 1; ++i) {
      dx[i] = x[i+1] - x[i];
      assert(dx[i] > 0);
      p[i]  = (y[i+1] - y[i])/dx[i];
   }

   // recur up
   spline->c[0] = 0;
   for (i = 0; i < N - 2; ++i) {
      spline->c[i+1] = (p[i+1] - p[i] - spline->c[i]*dx[i])/dx[i+1];
   }

   // recur down
   spline->c[N-2] /= 2;
   for (i = N - 3; i >= 0; --i) {
      spline->c[i] = (p[i+1] - p[i] - spline->c[i+1]*dx[i+1])/dx[i];
   }

   // calculate the b_i
   for (i = 0; i < N - 1; ++i) {
      spline->b[i] = p[i] - spline->c[i]*dx[i];
   }

   return spline;
}

// function to evaluate quadratic spline at point x_new
double spline2_eval(spline2* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   // calculate and return interpolated value
   double y_new = s->y[i] + s->b[i]*(x_new - s->x[i]) + s->c[i]*(x_new - s->x[i])*(x_new -  s->x[i]);
   return y_new;
}

// function to evaluate slope of quadratic spline at point x_new
double spline2_eval_deriv(spline2* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   // calculate and return derivative
   double deriv = s->b[i] + 2*(s->c[i])*(x_new - s->x[i]);
   return deriv;
}

// function to evaluate integral of quadratic spline up to point x_new
double spline2_eval_integral(spline2* s, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_array(s->N, s->x, x_new);

   
   // loop over all intervals up to (but not including) the last one
   double integral = 0;
   for (int j = 0; j < i; ++j) {
      double dx = s->x[j+1] - s->x[j];
      integral += s->y[j]*dx + s->b[j]*pow(dx, 2)/2 + s->c[j]*pow(dx, 3)/3;
   }

   // handle the last interval (j = i)
   double dx = x_new - s->x[i];
   integral += s->y[i]*dx + s->b[i]*pow(dx, 2)/2 + s->c[i]*pow(dx, 3)/3;
   return integral;
}

// function to free memory
void spline2_free(spline2* s) {
   free(s->x);
   free(s->y);
   free(s->b);
   free(s->c);
   free(s);
}

