#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_interp.h>
#include"../util.h"

//
// DECLARE FUNCTIONS (from spline2.c)
//

// structure for holding data and coefficients
typedef struct {
   int N;      // number of data points
   double* x;  // arrays
   double* y;  // .
   double* b;  // .
   double* c;  // .
} spline2;

// function for initializing the spline
spline2* spline2_init(int N, double* x, double* y);

// function to evaluate quadratic spline at point x_new
double spline2_eval(spline2* s, double x_new);

// function to evaluate slope of quadratic spline at point x_new
double spline2_eval_deriv(spline2* s, double x_new);

// function to evaluate integral of quadratic spline up to point x_new
double spline2_eval_integral(spline2* s, double x_new);

// function to free memory
void spline2_free(spline2* s);


//
// MAIN FUNCTION
//

int main() {

   //
   // READ DATA FROM FILE
   //

   // set length of vectors and allocate
   int N = 10;
   gsl_vector* x = gsl_vector_alloc(N);
   gsl_vector* y = gsl_vector_alloc(N);
   
   // read data from file and check that sizes match
   FILE* x_file = fopen("x.txt", "r");
   FILE* y_file = fopen("y.txt", "r");
   gsl_vector_fscanf(x_file, x);
   gsl_vector_fscanf(y_file, y);

   // also convert data to plain array (this is for gsl interpolation)
   double xa[x->size];
   double ya[y->size];
   for (int i = 0; i < x->size; ++i) {
      xa[i] = gsl_vector_get(x, i);
      ya[i] = gsl_vector_get(y, i);
   }

   //
   // QUADRATIC SPLINE
   //

   // initialize quadratic spline
   spline2* s = spline2_init(N, xa, ya);
   
   // plot homemade quadratic interpolation
   double x_min = gsl_vector_get(x, 0);
   double x_max = gsl_vector_get(x, x->size - 1);
   double step = 0.05;
   int n = (x_max - x_min)/step;
   FILE* spline2_file = fopen("spline2.txt", "w");
   for (int i = 0; i <= n; ++i) {
      double x_new = i*step;
      double y_new = spline2_eval(s, x_new);
      double d_new = spline2_eval_deriv(s, x_new);
      double i_new = spline2_eval_integral(s, x_new);
      fprintf(spline2_file, "%10.4f %10.4f %10.4f %10.4f\n", x_new, y_new, d_new, i_new);
   }

   // free memory and close files
   spline2_free(s);
   fclose(spline2_file);
   fclose(x_file);
   fclose(y_file);
   return 0;
}
