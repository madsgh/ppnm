#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include"../util.h"


//
// LINEAR INTERPOLATION
//

// function to return the interpolant at a point x_new
double spline1_eval(gsl_vector* x, gsl_vector* y, double x_new) { 

   // find interval
   int i = binsearch_vector(x, x_new);

   // calculate slope
   double p = (gsl_vector_get(y, i+1) - gsl_vector_get(y, i))/(gsl_vector_get(x, i+1) - gsl_vector_get(x, i));

   // calculate interpolated y value
   double y_new = gsl_vector_get(y, i) + p*(x_new - gsl_vector_get(x,i));

   return y_new;
}


// integrate linear interpolant up to some point x_new
double spline1_eval_integ (gsl_vector* x, gsl_vector* y, double x_new) {
   
   // find interval containing x_new
   int i = binsearch_vector(x, x_new);

   // loop over all intervals up to (but not including) the last one
   double integral = 0;
   for (int j = 0; j < i; ++j) {
      double x1 = gsl_vector_get(x, j);
      double y1 = gsl_vector_get(y, j);
      double x2 = gsl_vector_get(x, j+1);
      double y2 = gsl_vector_get(y, j+1);
      integral += (y2 + y1)*(x2 - x1)/2;
   }

   // handle the last interval (j = i)
   double x1 = gsl_vector_get(x, i);
   double y1 = gsl_vector_get(y, i);
   double y_new = spline1_eval(x, y, x_new);
   integral += (y_new + y1)*(x_new - x1)/2;

   return integral;
}
