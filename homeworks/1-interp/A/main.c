#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_interp.h>
#include"../util.h"

//
// DECLARE FUNCTIONS (from spline1.c)
//

// function to return the interpolant at a point x_new
double spline1_eval(gsl_vector* x, gsl_vector* y, double x_new);

// integrate linear interpolant up to some point x_new
double spline1_eval_integ(gsl_vector* x, gsl_vector* y, double x_new);


//
// MAIN FUNCTION
//

int main() {

   //
   // READ DATA FROM FILE
   //

   // set length of vectors and allocate
   int N = 10;
   gsl_vector* x = gsl_vector_alloc(N);
   gsl_vector* y = gsl_vector_alloc(N);
   
   // read data from file and check that sizes match
   FILE* x_file = fopen("x.txt", "r");
   FILE* y_file = fopen("y.txt", "r");
   gsl_vector_fscanf(x_file, x);
   gsl_vector_fscanf(y_file, y);

   // also convert data to plain array (this is for gsl interpolation)
   double xa[x->size];
   double ya[y->size];
   for (int i = 0; i < x->size; ++i) {
      xa[i] = gsl_vector_get(x, i);
      ya[i] = gsl_vector_get(y, i);
   }

   // 
   // LINEAR INTERPOLATION
   //

   // compute linear interpolant using gsl
   gsl_interp* gsl_linterp = gsl_interp_alloc(gsl_interp_linear, x->size);
   gsl_interp_init(gsl_linterp, xa, ya, x->size);
   gsl_interp_accel* acc = gsl_interp_accel_alloc();
   
   
   // compare homemade linterp with gsl_interp_linear
   double x_min = gsl_vector_get(x, 0);
   double x_max = gsl_vector_get(x, x->size - 1);
   double step = 0.05;
   int n = (x_max - x_min)/step;
   FILE* spline1_file = fopen("spline1.txt", "w");
   for (int i = 0; i <= n; ++i) {
      double x_new = i*step;
      double y_new = spline1_eval(x, y, x_new);
      double i_new = spline1_eval_integ(x, y, x_new);
      double y_gsl = gsl_interp_eval(gsl_linterp, xa, ya, x_new, acc);
      double i_gsl = gsl_interp_eval_integ(gsl_linterp, xa, ya, x_min, x_new, acc);
      fprintf(spline1_file, "%10.4f %10.4f %10.4f %10.4f %10.4f\n", x_new, y_gsl, y_new, i_gsl, i_new);
   }


   


   // free memory and close files
   gsl_interp_accel_free(acc);
   gsl_interp_free(gsl_linterp);
   fclose(spline1_file);
   fclose(x_file);
   fclose(y_file);
   return 0;
}
