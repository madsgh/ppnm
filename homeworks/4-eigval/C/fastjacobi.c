#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


// perform matrix product A <- A*J
void timesJ(gsl_matrix* A, int p, int q, double theta);

// perform matrix product A <- J*A
void Jtimes(gsl_matrix* A, int p, int q, double theta);

// perform product Jt*A*J using only upper diagnal
void JtAJ(gsl_matrix* A, int p, int q, double theta) {
   double c = cos(theta);
   double s = sin(theta);

   double apq = gsl_matrix_get(A, p, q);
   double app = gsl_matrix_get(A, p, p);
   double aqq = gsl_matrix_get(A, q, q);
   gsl_matrix_set(A, p, q, 0);
   gsl_matrix_set(A, p, p, c*c*app - 2*s*c*apq + s*s*aqq);
   gsl_matrix_set(A, q, q, s*s*app + 2*s*c*apq + c*c*aqq);

   // loop over remaining upper triangle elements in columns p, q and rows p, q
   for (int i = 0; i < p; ++i) {
      double aip = gsl_matrix_get(A, i, p);
      double aiq = gsl_matrix_get(A, i, q);
      gsl_matrix_set(A, i, p, c*aip - s*aiq);
      gsl_matrix_set(A, i, q, s*aip + c*aiq);
   }

   for (int i = p + 1; i < q; ++i) {
      double api = gsl_matrix_get(A, p, i);
      double aiq = gsl_matrix_get(A, i, q);
      gsl_matrix_set(A, p, i, c*api - s*aiq);
      gsl_matrix_set(A, i, q, s*api + c*aiq);
   }

   for (int i = q + 1; i < A->size1; ++i) {
      double api = gsl_matrix_get(A, p, i);
      double aqi = gsl_matrix_get(A, q, i);
      gsl_matrix_set(A, p, i, c*api - s*aqi);
      gsl_matrix_set(A, q, i, s*api + c*aqi);
   }

}


// diagonalize using jacobi rotation
void jacobi_diag_fast(gsl_matrix* A, gsl_matrix* V) {

   // sweep until convergence
   int n = A->size1;
   int changed;
   do {
      
      // reset check variable   
      changed = 0;

      // loop over elements above the diagonal
      for (int p = 0; p < n-1; p++) {
         for (int q = p+1; q < n; q++) {

            // prepare to rotate
            double apq = gsl_matrix_get(A, p, q);
            double app = gsl_matrix_get(A, p, p);
            double aqq = gsl_matrix_get(A, q, q);
            double theta = 0.5*atan2(2*apq, aqq-app);
            double c = cos(theta);
            double s = sin(theta);
            double new_app = c*c*app - 2*s*c*apq + s*s*aqq;
            double new_aqq = s*s*app + 2*s*c*apq + c*c*aqq;

            // do rotation 
            if (new_app != app || new_aqq != aqq) {
               changed = 1;
               JtAJ(A, p, q, theta);     // A <- Jt*A*J
               timesJ(V, p, q,  theta);  // V <- V*J
            }

         }
      }
   } while(changed != 0);
}
