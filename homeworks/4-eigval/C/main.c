#include<math.h>
#include<string.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_eigen.h>

// perform matrix product A <- A*J
void timesJ(gsl_matrix* A, int p, int q, double theta);

// perform matrix product A <- J*A
void Jtimes(gsl_matrix* A, int p, int q, double theta);

// diagonalize using jacobi rotations
void jacobi_diag(gsl_matrix* A, gsl_matrix* V);
void jacobi_diag_fast(gsl_matrix* A, gsl_matrix* V);

int main(int argc, char** argv) {

   // take input
   char* test = argv[1];
   int N = atoi(argv[2]);

   // make random symmetric gsl matrix A 
   unsigned int SEED = 123321;
   srandom(SEED);
   gsl_matrix* A = gsl_matrix_alloc(N, N);
   gsl_matrix* V = gsl_matrix_alloc(N, N);
   for (int i = 0; i < A->size1; ++i) {
      for (int j = i; j < A->size2; ++j) {
         double aij = 5*((double)random())/RAND_MAX;
         gsl_matrix_set(A, i, j, aij);
         gsl_matrix_set(A, j, i, aij);
      }
   }  


   if      (strcmp(test, "jacobi") == 0)     { jacobi_diag(A, V); }
   else if (strcmp(test, "fastjacobi") == 0) { jacobi_diag_fast(A, V); }
   else if (strcmp(test, "gsl") == 0)        { 
      gsl_vector* eigval = gsl_vector_alloc(N);
      gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(N);
      gsl_eigen_symmv(A, eigval, V, w);
      gsl_eigen_symmv_free(w);
   }
   else { printf("Unkown input!\n"); }

  
   return 0;
}
