#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// perform matrix product A <- A*J
void timesJ(gsl_matrix* A, int p, int q, double theta);

// perform matrix product A <- J*A
void Jtimes(gsl_matrix* A, int p, int q, double theta);

// diagonalize using jacobi rotation
void jacobi_diag(gsl_matrix* A, gsl_matrix* V);


int main() {

   // print task
   printf("====================\n");
   printf("====   TASK A   ====\n");
   printf("====================\n\n");


   // make random symmetric gsl matrix A 
   int N = 5;
   unsigned int SEED = time(NULL);
   srandom(SEED);
   gsl_matrix* A = gsl_matrix_alloc(N, N);
   for (int i = 0; i < A->size1; ++i) {
      for (int j = i; j < A->size2; ++j) {
         double aij = 5*((double)random())/RAND_MAX;
         gsl_matrix_set(A, i, j, aij);
         gsl_matrix_set(A, j, i, aij);
      }
   }  

   // also make copy to do checks afterwards
   gsl_matrix* Acopy = gsl_matrix_alloc(N, N);
   gsl_matrix_memcpy(Acopy, A);

   // print original A matrix
   printf("This is the original A matrix (should be symmetric):\n");
   matrix_print(A);

   // do diagonalization and check results
   gsl_matrix* V = gsl_matrix_alloc(N, N);
   gsl_matrix_set_identity(V);
   jacobi_diag(A, V);

   printf("This is D (should be diagonal):\n");
   matrix_print(A);

   printf("This is Vt*V (should be identity):\n");
   gsl_matrix* VtV = gsl_matrix_alloc(N, N);
   gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, V, V, 0, VtV);
   matrix_print(VtV);

   printf("This is Vt*A*V (should be same as D):\n");
   gsl_matrix* VtAV = gsl_matrix_alloc(N, N);
   gsl_matrix* temp = gsl_matrix_alloc(N, N);
   gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, Acopy, V, 0, temp);
   gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, V, temp, 0, VtAV);
   matrix_print(VtAV);

   printf("This is V*D*Vt (should be same as A):\n");
   gsl_matrix* VDVt = gsl_matrix_alloc(N, N);
   gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, A, V, 0, temp);
   gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, V, temp, 0, VDVt);
   matrix_print(VDVt);

   // free memory
   gsl_matrix_free(A);
   gsl_matrix_free(Acopy);
   gsl_matrix_free(V);
   gsl_matrix_free(temp);
   gsl_matrix_free(VtAV);
   gsl_matrix_free(VDVt);

   return 0;
}
