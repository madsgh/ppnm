#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// perform matrix product A <- A*J
void timesJ(gsl_matrix* A, int p, int q, double theta);

// perform matrix product A <- J*A
void Jtimes(gsl_matrix* A, int p, int q, double theta);

// diagonalize using jacobi rotation
void jacobi_diag(gsl_matrix* A, gsl_matrix* V);


int main() {

   // print task
   printf("====================\n");
   printf("====   TASK B   ====\n");
   printf("====================\n\n");


   // make Hamiltonian matrix
   int N = 100;
   double s = 1.0/(N+1);
   double k = -1/(s*s);
   double scale = sqrt(2.0/(N + 1));
   gsl_matrix* H = gsl_matrix_alloc(N, N);

   gsl_matrix_set(H, 0, 0, -2*k);
   gsl_matrix_set(H, 0, 1,  1*k);

   for (int i = 1; i < N-1; ++i) {
      gsl_matrix_set(H, i, i-1,  1*k);
      gsl_matrix_set(H, i, i  , -2*k);
      gsl_matrix_set(H, i, i+1,  1*k);
   }  

   gsl_matrix_set(H, N-1, N-2,  1*k);
   gsl_matrix_set(H, N-1, N-1, -2*k);

   // do diagonalization and check results
   gsl_matrix* V = gsl_matrix_alloc(N, N);
   gsl_matrix_set_identity(V);
   jacobi_diag(H, V);

   // check energies
   printf("Particle in a box energies:\n");
   for (int k = 0; k < 10; ++k) {
      double exact = M_PI*M_PI*(k + 1)*(k + 1);
      double calc  = gsl_matrix_get(H, k, k);
      printf("n = %2d: exact = %10.6f, calc = %10.6f\n", k+1, exact, calc);
   }
   
   // change phase of eigenvector to the conventional one
   for (int i = 0; i < N; ++i) {
      gsl_vector_view view = gsl_matrix_column(V, i);
      gsl_vector* vi = &view.vector;
      double first = gsl_vector_get(vi,0);
      if (first < 0) {
         gsl_vector_scale(vi, -1);
      }
   }

   // print eigenfunctions for plotting
   FILE* eigfunc_file = fopen("eigfunc.txt", "w");
   double x;
   int nplots = 3;

   fprintf(eigfunc_file, "0 ");
   for (int k = 0; k < nplots; ++k) {
      fprintf(eigfunc_file, "0 0 ");
   }
   fprintf(eigfunc_file, "\n");

   for(int i = 0; i < N; i++) {
      x = (i + 1.0)/(N + 1);
      fprintf(eigfunc_file, "%g ", x);
      for (int k = 0; k < nplots; ++k) {
         fprintf(eigfunc_file, "%g %g ", sin((k+1)*M_PI*x), gsl_matrix_get(V, i, k)/scale);
      }
      fprintf(eigfunc_file, "\n");
   }

   fprintf(eigfunc_file, "1 ");
   for (int k = 0; k < nplots; ++k) {
      fprintf(eigfunc_file, "0 0 ");
   }
   fprintf(eigfunc_file, "\n");
   

   // free memory and close files
   gsl_matrix_free(H);
   gsl_matrix_free(V);
   fclose(eigfunc_file);

   return 0;
}
