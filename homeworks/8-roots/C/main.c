#include<stdio.h>
#include<math.h>
#include<float.h>
#include<string.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"../util.h"

// driver function
int odedriver( void (*f)(int n, double x, double* y, double* dydx) // right-hand-side of dy/dx = f(x, y) 
             , int n           // size of vectors 
	          , double  a       // the start-point a 
	          , double  b       // the end-point of the integration 
	          , double* ya      // y(a) 
	          , double* yb      // y(b) to be calculated 
	          , double  h       // initial step-size 
	          , double  acc     // absolute accuracy goal 
	          , double  eps     // relative accuracy goal 
             , char*   outfile // trajectory file
             , int     verb    // verbose output?
             , int     print   // print trajectory?
             ); 

// newton solver
void newton( void (*f)(gsl_vector* x, gsl_vector* y)
           , gsl_vector* x0
           , double eps
           , double h
           , int maxiter
           , gsl_vector* result
           , gsl_matrix* R
           , int verb
           );

// rhs for schrödinger equation
double e;
void f(int n, double x, double* y, double* dydx) {
   dydx[0] = y[1];
   dydx[1] = -2*(e + 1.0/x)*y[0];
}


// aux function for simple boundary condition
double rmax;
char* outfile;
char* boundary;
void g(gsl_vector* x, gsl_vector* y) { 

   // declare and set variables for odedriver
   e = gsl_vector_get(x, 0);
   int n = 2; 
   double a = 1e-16;  // avoid division by zero
   double b = rmax;
   double ya[n];
   double yb[n];
   double h = 1e-7;
   double acc = 1e-6;
   double eps = 1e-6;
   int verb = 0;
   int print = 0;
   double target;

   // set initial values
   ya[0] = 0;  // value of wf at r = 0
   ya[1] = 1;  // derivative of wf at r = 0

   // call driver
   odedriver(&f, n, a, b, ya, yb, h, acc, eps, outfile, verb, print);

   // target value
   if (strcmp(boundary, "simple") == 0) target = 0; 
   if (strcmp(boundary, "better") == 0) target = rmax*exp(-sqrt(-2*e)*rmax); 

   // write value of objective function
   gsl_vector_set(y, 0, yb[0] - target);

}


int main(int argc, char** argv) {

   // take input
   rmax = atof(argv[1]);
   boundary = argv[2];
   outfile = "hydrogen.txt";
   
   // set up problem 
   int n = 1;
   gsl_matrix* J  = gsl_matrix_alloc(n, n);
   gsl_matrix* R  = gsl_matrix_alloc(n, n);
   gsl_vector* result = gsl_vector_alloc(n); 
   gsl_vector* x0     = gsl_vector_alloc(n); 
   gsl_vector_set(x0, 0, -1);
   double eps  = 1e-6;
   int maxiter = 100;
   int verbose = 0;
   double h    = 1.0*sqrt(DBL_EPSILON);
   
   // call newton solver 
   newton(g, x0, eps, h, maxiter, result, R, verbose);
   printf("%8.4f  %.12f\n", rmax, gsl_vector_get(result, 0));
  
   // free memory
   gsl_matrix_free(J);
   gsl_matrix_free(R);
   gsl_vector_free(x0);
   gsl_vector_free(result);

   return 0;
}
