#include<stdio.h>
#include<math.h>
#include<float.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// jacobian matrix and newton solver
void jacobian(void (*f)(gsl_vector* x, gsl_vector* y), gsl_vector* x0, gsl_matrix* J, double h);
void   newton(void (*f)(gsl_vector* x, gsl_vector* y), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, gsl_matrix* R, int verb);

// functions for solving linear equations
void GS_decomp(gsl_matrix* A, gsl_matrix* R);
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

// function with two components and two variables
void f(gsl_vector* x, gsl_vector* y) {
   double x0 = gsl_vector_get(x, 0);
   double x1 = gsl_vector_get(x, 1);
   gsl_vector_set(y, 0, x0*x0);
   gsl_vector_set(y, 1, x1*x1);
}

// gradient of rosenbrock function
void g(gsl_vector* x, gsl_vector* y) {
   double x0 = gsl_vector_get(x, 0);
   double x1 = gsl_vector_get(x, 1);
   gsl_vector_set(y, 0, 400*x0*x0*x0 + (2 - 400*x1)*x0 - 2);
   gsl_vector_set(y, 1, 200*x1 - 200*x0*x0);
}


// do stuff
int main() {
   
   // set up problem 
   printf("Searching for minimum of Rosenbrock function f(x,y) = (1 - x)**2 + 100*(y - x**2)**2\n");
   int n = 2;
   gsl_matrix* J  = gsl_matrix_alloc(n, n);
   gsl_matrix* R  = gsl_matrix_alloc(n, n);
   gsl_vector* result = gsl_vector_alloc(n); 
   gsl_vector* x0     = gsl_vector_alloc(n); 
   gsl_vector_set(x0, 0, 1.1);
   gsl_vector_set(x0, 1, 1.1);
   double eps  = 1e-6;
   int maxiter = 100;
   int verbose = 1;
   double h    = 1.0*sqrt(DBL_EPSILON);
   printf("Step-size for finite differences: h = %g\n\n", h);
   
   // call newton solver 
   newton(g, x0, eps, h, maxiter, result, R, verbose);
   printf("\nThe minimum is at (x,y) = \n");
   vector_print(result);
  
   // free memory
   gsl_matrix_free(J);
   gsl_matrix_free(R);
   gsl_vector_free(x0);
   gsl_vector_free(result);
   



   return 0;
}
