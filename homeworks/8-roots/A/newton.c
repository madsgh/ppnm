#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<stdarg.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"../util.h"

// functions for solving linear equations etc.
void GS_decomp(gsl_matrix* A, gsl_matrix* R);
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
double norm(gsl_vector* x);

// calculate jacobian matrix with Jij = dfi/dxj
void jacobian(void (*f)(gsl_vector* x, gsl_vector* y), gsl_vector* x0, gsl_vector* y0, gsl_matrix* J, double h) {

   // get size of vectors
   int n = x0->size;


   // loop over all columns in jacobian
   gsl_vector* x1 = gsl_vector_alloc(n);
   gsl_vector* y1 = gsl_vector_alloc(n);
   for (int j = 0; j < n; ++j) {
      gsl_vector_set_basis(x1, j);     // x1              = (0, ..., 1, ..., 0) (non-zero in the jth entry)
      gsl_vector_axpby(1, x0, h, x1);  // x1 <- x0 + h*x1 = (0, ..., h, ..., 0) (non-zero in the jth entry)
      f(x1, y1);                       // y1 <- f(x1)
      gsl_vector_axpby(-1, y0, 1, y1); // y1 <- y1 - y0
      gsl_vector_scale(y1, 1.0/h);     // y1 <- y1/h = (y1 - y0)/h
      gsl_matrix_set_col(J, j, y1);    // J[:][j] = y1 (set jth column of jacobian)
   }

   // free vectors
   gsl_vector_free(y1);
   gsl_vector_free(x1);

}


// find root using newton's methods
void newton(void (*f)(gsl_vector* x, gsl_vector* y), gsl_vector* x0, double eps, double h, int maxiter, gsl_vector* result, gsl_matrix* R, int verb) {
   
   // set counter
   int iter = 0;

   // get size of vectors and allocate
   int n = x0->size;
   gsl_matrix* J    = gsl_matrix_alloc(n, n);
   gsl_vector* y0   = gsl_vector_alloc(n);
   gsl_vector* xnew = gsl_vector_alloc(n);
   gsl_vector* ynew = gsl_vector_alloc(n);
   gsl_vector* dx   = gsl_vector_alloc(n);

   // loop until convergence
   do {

      // check that we are not exceeding max number of iterations
      assert(iter < maxiter && "Too many iterations!");
      iter++;

      // get function at x0 
      f(x0, y0);
      if (verb) printf("\n=========== Iteration %d ===========\n", iter);
      if (verb) printf("x0 = \n");
      if (verb) vector_print(x0);
      if (verb) printf("y0 = \n");
      if (verb) vector_print(y0);
      if (verb) printf("||y0|| = %.6f\n\n", norm(y0));


      // calculate jacobian
      jacobian(f, x0, y0, J, h);
      if (verb) printf("J =\n");
      if (verb) matrix_print(J);

      // calculate newton step (solve J*dx = -y0)
      GS_decomp(J, R);
      gsl_vector_scale(y0, -1.0); // y0 <- -y0 = -f(x0)
      GS_solve(J, R, y0, dx);     // solve J*dx = -f(x0)

      if (verb) printf("newton step dx =\n");
      if (verb) vector_print(dx);

      // do simple backtracking line search
      if (verb) printf("****** Starting line search ******\n");
      float lambda = 2.0;
      do {
         lambda /= 2;
         gsl_vector_axpby(1.0, x0, 0, xnew);    // xnew <- x0
         gsl_vector_axpby(lambda, dx, 1, xnew); // xnew <- lambda*dx + xnew = lambda*dx + x0
         f(xnew, ynew);                         // ynew <- f(xnew) = f(x0 + lambda*dx)
         if (verb) printf("xnew = x0 + %.4f*dx = \n", lambda);
         if (verb) vector_print(xnew);
         if (verb) printf("||ynew||              = %.8f\n", norm(ynew));
         if (verb) printf("(1 - lambda/2)*||y0|| = %.8f\n\n", (1.0 - lambda/2.0)*norm(y0));
      } while (norm(ynew) > (1.0 - lambda/2.0)*norm(y0) && lambda > 1.0/64);
      if (verb) printf("******   Line search done   ******\n");

   // make the step
   gsl_vector_memcpy(x0, xnew);

   } while (norm(ynew) > eps && norm(dx) > h);

   // when converged do this 
   gsl_vector_memcpy(result, x0);   // copy result into result vector
   f(result, y0);                   // call target function on root (useful if the target function makes a plot)
   if (verb) printf("\n---------------------------------------------\n");
   if (verb) printf("Newton solver converged after %3d iterations!\n", iter);
   if (verb) printf("---------------------------------------------\n");

   // free memory
   gsl_vector_free(y0);
   gsl_vector_free(dx);
   gsl_vector_free(xnew);
   gsl_vector_free(ynew);
   gsl_matrix_free(J);
   
}
