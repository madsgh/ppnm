#include <stdio.h> 
#include <math.h>
#include <complex.h>

// Stepper function (defined in rungekutta.c)
void rkstep23
   (  void (*f)(int n, double complex t, double complex* y, double complex* dydt)
   ,  int n
   ,  double complex h
   ,  double complex t
   ,  double complex* y_curr
   ,  double complex* y_next
   ,  double complex* err
   );

// Driver function (defined in rungekutta.c)
int odedriver
   (  void (*f)(int n, double complex t, double complex* y, double complex* dydt) // right-hand-side of dy/dt = f(t, y) 
   ,  int n                // size of vectors 
	,  double complex  a    // the start-point of the integration
	,  double complex  b    // the end-point of the integration 
	,  double complex* ya   // y(a) 
	,  double complex* yb   // y(b) (to be calculated)
	,  double absh          // norm of initial step
	,  double acc           // absolute accuracy goal 
	,  double eps           // relative accuracy goal 
   ,  char*  outfile       // trajectory file name
   ,  int    verb          // print verbose output (i.e. one file per step)?
   );

// RHS function for u'' = -u
void f(int n, double complex t, double complex* y, double complex* dydt) {
   dydt[0] =  y[1];
   dydt[1] = -y[0];
}

// RHS function for u' = i*u
void g(int n, double complex t, double complex* y, double complex* dydt) {
   dydt[0] = I*y[0];
}

// RHS function for u' = -i*H*u (harmonic oscillator TDSE)
double A, B, omega;
void h(int n, double complex t, double complex* y, double complex* dydt) {
   double delta = (B - A)/(n - 1);
   double xi;
   double complex derivi;
   for (int i = 0; i < n; ++i) {
      xi = A + delta*i;
      if (i == 0) {
         derivi = (exp(-pow(A - delta, 2)) - 2*y[i] + y[i + 1])/(delta*delta);
      }
      else if (i == n - 1) {
         derivi = (y[i - 1] - 2*y[i] + exp(-pow(B + delta, 2)))/(delta*delta);
      }
      else {
         derivi = (y[i - 1] - 2*y[i] + y[i + 1])/(delta*delta);
      }
      dydt[i] = I/2*(derivi - omega*omega*xi*xi*y[i]);
   }
}

// Run the test cases
int main() {
   
   /**********************************************************************
    * Test case 1: u'' = -u (real initial values, along real line)
    * 
    * The system is equivalent to
    * [ y0' ] = [  y1 ]
    * [ y1' ] = [ -y0 ]
    * with y0 = u and y1 = u'.
    * 
    * Integration path : 0 to 2*pi
    * Initial values   : u(0) = 1, u'(0) = 0
    * Solution         : u(t) = cos(t), u'(t) = -sin(t)
    *********************************************************************/
   {
   // Declare and set variables for odedriver
   int n = 2; 
   double complex a = 0;
   double complex b = 2*M_PI;
   double complex ya[n];
   double complex yb[n];
   double absh = 0.001;
   double acc  = 1e-4;
   double eps  = 1e-4;
   char* outfile = "cosine.txt";

   // Set initial values
   ya[0] = 1;
   ya[1] = 0;

   // Call driver
   odedriver(&f, n, a, b, ya, yb, absh, acc, eps, outfile, 0);
   }

   /**********************************************************************
    * Test case 2: u'' = -u (complex initial values, along real line)
    * 
    * Integration path : 0 to 2*pi
    * Initial values   : u(0) = i, u'(0) = 0
    * Solution         : u(t) = i*cos(t), u'(t) = -i*sin(t)
    *********************************************************************/
   {
   // Declare and set variables for odedriver
   int n = 2; 
   double complex a = 0;
   double complex b = 2*M_PI;
   double complex ya[n];
   double complex yb[n];
   double absh = 0.001;
   double acc  = 1e-4;
   double eps  = 1e-4;
   char* outfile = "icosine.txt";

   // Set initial values
   ya[0] = I;
   ya[1] = 0;

   // Call driver
   odedriver(&f, n, a, b, ya, yb, absh, acc, eps, outfile, 0);
   }

   /**********************************************************************
    * Test case 3: u' = i*u (complex initial value, along complex line)
    * 
    * Integration path : 0 to 2*pi*(i - 1)/sqrt(2)
    * Initial values   : u(0) = i
    * Solution         : u(t) = i*exp(i*t)
    *********************************************************************/
   {
   // Declare and set variables for odedriver
   int n = 1; 
   double complex a = 0;
   double complex b = 2*M_PI*(I - 1);
   double complex ya[n];
   double complex yb[n];
   double absh = 0.001;
   double acc  = 1e-4;
   double eps  = 1e-4;
   char* outfile = "exp.txt";

   // Set initial values
   ya[0] = I;
   ya[1] = 0;

   // Call driver
   odedriver(&g, n, a, b, ya, yb, absh, acc, eps, outfile, 0);
   }


   /**********************************************************************
    * Test case 4: u' = -i*H*u (harmonic oscillator TDSE on a grid)
    * 
    * Integration path : 0 to 5
    * Initial values   : u(x, t = 0) = exp(-(x - 2)**2) (shifted Gaussian)
    * Solution         : Wave-packet moving back and forth
    *********************************************************************/
   {
   // Declare and set variables for odedriver
   int n = 101; 
   double complex a = 0;
   double complex b = 5;
   double complex ya[n];
   double complex yb[n];
   double absh = 0.001;
   double acc  = 1e-4;
   double eps  = 1e-4;
   char* outfile = "ho/harmonic.txt";

   // Set parameters
   A = -10.0;
   B =  10.0;
   double delta = (B - A)/(n - 1);
   omega = 1.0;

   // Set initial wave function and print x-values to file
   FILE* xfile = fopen("ho/xvalues.txt", "w");
   fprintf(xfile, "#x values\n");
   for (int i = 0; i < n; ++i) {
      double xi = A + delta*i;
      ya[i] = exp(-pow(xi - 2, 2));
      fprintf(xfile, "% .12e\n", xi);
   }
   fclose(xfile);

   // Call driver
   odedriver(&h, n, a, b, ya, yb, absh, acc, eps, outfile, 1);
   }

   return 0;
}
