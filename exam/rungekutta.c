#include <stdio.h> 
#include <math.h>
#include <complex.h>

/********************************************************************************
 * Runge-Kutta (2,3) stepper function
 *******************************************************************************/
void rkstep23
   (  void (*f)(int n, double complex t, double complex* y, double complex* dydt)
   ,  int n                   // size of vector
   ,  double complex h        // step
   ,  double complex t        // current 'time'
   ,  double complex* y_curr  // current y values
   ,  double complex* y_next  // estimate of y(t + h)
   ,  double complex* err     // error estimate vector
   ) 
{
   // Declare local variables
   double complex k1[n];
   double complex k2[n];
   double complex k3[n];
   double complex k4[n];
   double complex y_temp[n];

   // Calculate first point
   f(n, t, y_curr, k1);
   for (int i = 0; i < n; ++i) {
      y_temp[i] = y_curr[i] + (1.0/2)*k1[i]*h;
   }

   // Calculate first point
   f(n, t + (1.0/2)*h, y_temp, k2);
   for (int i = 0; i < n; ++i) {
      y_temp[i] = y_curr[i] + (3.0/4)*k2[i]*h;
   }

   // Calculate third point and second order estimate of y(x + h)
   f(n, t + (3.0/4)*h, y_temp, k3);
   for (int i = 0; i < n; ++i) {
      y_temp[i] = y_curr[i] + ((2.0/9)*k1[i] + (1.0/3)*k2[i] + (4.0/9)*k3[i])*h;
   }

   // Calculate fourth point, third order estimate of y(x + h) and error estimate
   f(n, t + h, y_temp, k4);
   for (int i = 0; i < n; ++i) {
      y_next[i] = y_curr[i] + ((7.0/24)*k1[i] + (1./4)*k2[i] + (1.0/3)*k3[i] + (1.0/8)*k4[i])*h;
      err[i] = y_next[i] - y_temp[i];
   }

} /* end rkstep */

/********************************************************************************
 * Driver function
 *******************************************************************************/
int odedriver
   (  void (*f)(int n, double complex t, double complex* y, double complex* dydt) // right-hand-side of dy/dt = f(t, y) 
   ,  int n                // size of vectors 
	,  double complex  a    // the start-point of the integration
	,  double complex  b    // the end-point of the integration 
	,  double complex* ya   // y(a) 
	,  double complex* yb   // y(b) (to be calculated)
	,  double absh          // norm of initial step
	,  double acc           // absolute accuracy goal 
	,  double eps           // relative accuracy goal 
   ,  char*  outfile       // trajectory file name
   ,  int    verb          // print verbose output (i.e. one file per step)?
   ) 
{ 
   // Declare local variables
   int k = 0;                                   // step counter
   double complex h = absh*(b - a)/cabs(b - a); // initial step
   double complex t;                            // current t
   double complex y[n];                         // current y vector
   double complex yh[n];                        // estimate of y(t + h)
   double complex err[n];                       // error estimate vector
   double sum;                                  // intermediate variable
   double tau;                                  // local tolerance
   FILE* file = fopen(outfile, "w");

   // Write header to file (only non-verbose case)
   if (!verb) {
      fprintf(file, "%-20s %-19s     ", "#Re(t)", "Im(t)");
      for (int i = 0; i < n; ++i) {
         char re[100]; char im[100];
         sprintf(re, "Re(y%d)", i); 
         sprintf(im, "Im(y%d)", i);
         fprintf(file, "%-19s %-19s     ", re, im);
      }
      fprintf(file, "\n");
   }
   
   // Prepare first step and write it to file
   t = a;
   if (!verb) {
      fprintf(file, "% .12e % .12e     ", creal(t), cimag(t));
      for (int i = 0; i < n; ++i) {
         y[i] = ya[i];
         fprintf(file, "% .12e % .12e     ", creal(y[i]), cimag(y[i]));
      }
      fprintf(file, "\n");
   }
   else {
      fprintf(file, "#t values\n");
      fprintf(file, "% .12e % .12e\n", creal(t), cimag(t));
      char string[100];
      sprintf(string, "%s_%d", outfile, k);
      FILE* verbfile = fopen(string, "w");
      fprintf(verbfile, "#y values for t = (% .12e,% .12e)\n", creal(t), cimag(t));
      for (int i = 0; i < n; ++i) {
         y[i] = ya[i];
         fprintf(verbfile, "% .12e % .12e\n", creal(y[i]), cimag(y[i]));
      }
      fclose(verbfile);
   }


   // Loop until endpoint is reached 
   while (cabs(t - a) < cabs(b - a)) {

      // Avoid overstepping
      if (cabs(t + h - a) > cabs(b - a)) {
         h = b - t;
      }

      // Make step
      rkstep23(f, n, h, t, y, yh, err);
      
      // Calculate local error as norm of error estimate vector
      sum = 0;
      for (int i = 0; i < n; ++i) {
         sum += cabs(err[i])*cabs(err[i]);
      }  
      double e = sqrt(sum);

      // Calculate norm of estimated y(t + h)
      sum = 0;
      for (int i = 0; i < n; ++i) {
         sum += cabs(yh[i])*cabs(yh[i]);
      }
      double norm = sqrt(sum);

      // Calculate local tolerance
      tau = (norm*eps + acc)*sqrt(cabs(h/(b - a))); 

      // Accept or reject step?
      if (e < tau) {
         k++;     // increment counter
         t += h;  // increment t

         // Increment elements of y vector and print to file
         if (!verb) {
            fprintf(file, "% .12e % .12e     ", creal(t), cimag(t));
            for (int i = 0; i < n; ++i) {
               y[i] = yh[i];
               fprintf(file, "% .12e % .12e     ", creal(y[i]), cimag(y[i]));
            }
            fprintf(file, "\n");
         }
         else {
            fprintf(file, "% .12e % .12e\n", creal(t), cimag(t));
            char string[100];
            sprintf(string, "%s_%d", outfile, k);
            FILE* verbfile = fopen(string, "w");
            fprintf(verbfile, "#y values for t = (% .12e,% .12e)\n", creal(t), cimag(t));
            for (int i = 0; i < n; ++i) {
               y[i] = yh[i];
               fprintf(verbfile, "% .12e % .12e\n", creal(y[i]), cimag(y[i]));
            }
            fclose(verbfile);
         }
      }
      
      // Adjust step size
      if (e > 0) { h *= pow(tau/e, 0.25)*0.95; }
      else       { h *= 2; } 
     
   } /* end while */

   // Set final y vector
   for (int i = 0; i < n; ++i) {
      y[i] = yh[i];
   }

   // Close output file
   fclose(file);

   // Return number of steps
   return k;

} /* end odedriver */

