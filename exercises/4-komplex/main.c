#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main() {
	
	printf("\n\nTesting komplex_add:\n");
    komplex a = komplex_new(1,2);
    komplex b = komplex_new(3,4);
    komplex apb; komplex_set(&apb, 4, 6);
	komplex plus = komplex_add(a,b);
	komplex_print("a =",a);
	komplex_print("b =",b);
	komplex_print("a+b should   = ", apb);
	komplex_print("a+b actually = ", plus);

    printf("\n\nTesting komplex_sub:\n");
    komplex amb; komplex_set(&amb, -2, -2);
	komplex minus = komplex_sub(a,b);
	komplex_print("a =",a);
	komplex_print("b =",b);
	komplex_print("a+b should   = ", amb);
	komplex_print("a+b actually = ", minus);


}