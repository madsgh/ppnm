#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_sf_erf.h>
#include<gsl/gsl_sf_bessel.h>

//
// TASK A
//

// some function f
double f(double x, void* params) {
   return log(x)/sqrt(x);
}

// function that integrates f
double integral() {

   // define gsl function for the integrator to work with
   gsl_function F;
   F.function = &f;
   
   // max number of steps in the integration
   int limit = 999;

   // allocate memory for the integrator (w is a pointer to a 'workspace')
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

   // integration limits
   double a = 0;
   double b = 1;

   // error limits (absolute and relative)
   double epsabs = 1e-6;
   double epsrel = 1e-6;

   // declare variables to hold result and error estimate
   double result;
   double error;

   // do the integration, free memory and return result
   gsl_integration_qags(&F, a, b, epsabs, epsrel, limit, w, &result, &error);
	gsl_integration_workspace_free(w);
	return result;

}



// 
// TASK B
//

// kernel of the error function
double g(double x, void* params) {
   return (2/sqrt(M_PI)) * exp(-x*x);
}

// function that integrates g
double myerror(double z) {

   // define gsl function for the integrator to work with
   gsl_function G;
   G.function = &g;
   
   // max number of steps in the integration
   int limit = 999;

   // allocate memory for the integrator (w is a pointer to a 'workspace')
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

   // integration limits
   double a = 0;
   double b = z;

   // error limits (absolute and relative)
   double epsabs = 1e-6;
   double epsrel = 1e-6;

   // integration routine (2 = 21 point Gauss-Kronrod)
   int key = 2;

   // declare variables to hold result and error estimate
   double result;
   double error;

   // do the integration, free memory and return result
   gsl_integration_qag(&G, a, b, epsabs, epsrel, limit, key, w, &result, &error);
	gsl_integration_workspace_free(w);
	return result;

}



//
// TASK C
//

// define struct to hold multiple parameters
typedef struct {
   double n;
   double x;
} bessel_params;

// kernel of the Bessel function
double h(double tau, void* p) {

   // extract parameters
   bessel_params params = *(bessel_params*)p;
   double n = params.n;
   double x = params.x;

   return (1/M_PI) * cos(n*tau - x*sin(tau));
}

// function that integrates h
double bessel(bessel_params params) {

   // define gsl function for the integrator to work with
   gsl_function H;
   H.function = &h;
   H.params = &params;
   
   // max number of steps in the integration
   int limit = 999;

   // allocate memory for the integrator (w is a pointer to a 'workspace')
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

   // integration limits
   double a = 0;
   double b = M_PI;

   // error limits (absolute and relative)
   double epsabs = 1e-6;
   double epsrel = 1e-6;

   // integration routine (2 = 21 point Gauss-Kronrod)
   int key = 2;

   // declare variables to hold result and error estimate
   double result;
   double error;

   // do the integration, free memory and return result
   gsl_integration_qag(&H, a, b, epsabs, epsrel, limit, key, w, &result, &error);
	gsl_integration_workspace_free(w);
	return result;

}


//
// RUN MAIN
//


// main function
int main() {
   
   // do the integral of f from 0 to 1 and print the result
   double result = integral();
   printf("\nIntegral in task A = %g\n\n", result);

   // print error function
   double step = 0.1;
   double min = -3;
   double max = +3;
   FILE* error_file = fopen("data_error.txt", "w");
   for (double z = min; z <= max; z += step) {
      fprintf(error_file, "%10.6f %10.6f %10.6f\n", z, gsl_sf_erf(z), myerror(z));
   }

   // print bessel function
   bessel_params params;
   params.n = 0;
   step = 0.1;
   min =  0;
   max = 20;
   FILE* bessel_file = fopen("data_bessel.txt", "w");

   // loop over x
   for (double x = min; x <= max; x += step) {
      
      // print x
      params.x = x;
      fprintf(bessel_file, "\n%10.6f ", x);

      // loop over n
      for (int n = 0; n <= 4; n += 1) {
         
         // print bessel(n,x) and gsl_sf_bessel(n,x)
         params.n = n;
         fprintf(bessel_file, "%10.6f %10.6f ", gsl_sf_bessel_Jn(n, x), bessel(params));
      }

   }

 
   


   fclose(error_file);
   fclose(bessel_file);
   return 0;

}
