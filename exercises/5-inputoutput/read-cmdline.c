#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main(int argc, char** argv){

   printf("Take input from command line:\n");
   printf("%10s %10s %10s\n", "x", "sin(x)", "cos(x)");
   
   for (int i = 1; i < argc; i++) {
      double x = atof(argv[i]);
      printf("%10.4f %10.4f %10.4f\n", x, sin(x), cos(x));
   }

   return 0;
}
