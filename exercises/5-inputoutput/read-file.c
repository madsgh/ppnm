#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main(int argc, char** argv) {
 
   // get filenames and open files
   char* inp_name = argv[1];
   char* out_name = argv[2]; 

   FILE* inp_file = fopen(inp_name, "r");
   FILE* out_file = fopen(out_name, "a");


   // write header to file
   fprintf(out_file, "\nTake input from file:\n");
   fprintf(out_file, "%10s %10s %10s\n", "x", "sin(x)", "cos(x)");
	
   // read numbers x and write x, sin(x) and cos(x)
   double x;
	int items;
   while ( (items = fscanf(inp_file, "%lg", &x)) != EOF) {
      
      // output to file
      fprintf(out_file, "%10.4f %10.4f %10.4f\n", x, sin(x), cos(x));

	} 
  

   // close files
   fclose(inp_file);
   fclose(out_file);

   return 0;
}
