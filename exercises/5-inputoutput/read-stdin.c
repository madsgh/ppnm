#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main() {
  
   // to stdout
   printf("\nTake input from stdin:\n"); 
   fprintf(stdout, "%10s %10s %10s\n", "x", "sin(x)", "cos(x)");
	
   // read numbers x and write x, sin(x) and cos(x)
   double x;
	int items;
   while ( (items = fscanf(stdin, "%lg", &x)) != EOF) {
      
      // output to file
      fprintf(stdout, "%10.4f %10.4f %10.4f\n", x, sin(x), cos(x));

	} 
   
   return 0;
}
