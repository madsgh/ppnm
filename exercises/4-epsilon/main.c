#include<stdio.h>
#include<limits.h>
#include<float.h>

int equal(double a, double b, double tau, double epsilon); 
void name_digit(int i);

int main(){
   
   /***************************************************************************
    * TASK 1 -- INT_MAX, INT_MIN and machine precision
    **************************************************************************/

   /***** TASK 1 (i) -- INT_MAX *****/
   printf("\n\nTASK 1 (i): INT_MAX\n");
   int i = 0;

   // Only works when compiling with clang without the -O option!
   while(i+1>i) i++;
   printf("Maximum integer from while loop = %d\n", i);
   printf("Maximum integer from INT_MAX    = %d\n", INT_MAX);

   
   /***** TASK 1 (ii) -- INT_MIN *****/
   printf("\n\nTASK 1 (ii): INT_MIN\n");
   printf("I skipped this one...\n");


   /***** TASK 1 (iii) -- machine epsilon *****/
   printf("\n\nTASK 1 (iii): MACINE EPSILON\n");
   printf("Machine precision using while loop:\n");

   /* float */
   float x = 1;
   while(1+x != 1){ x /= 2; }
   x *= 2;
   printf("eps float  = %.10g\n", x);
   
   /* double */
   double y = 1;
   while(1+y != 1){ y /= 2; }
   y *= 2;
   printf("eps double = %.10lg\n", y);
  
   /* long double */
   long double z = 1;
   while(1+z != 1){ z /= 2; }
   z *= 2;
   printf("eps long   = %.10Lg\n", z);


   printf("\nMachine precision using for loop:\n");

   /* float */
   float e;
   for(e = 1; 1+e != 1; e/=2){}
   e *= 2;
   printf("eps float  = %.10g\n", e);
   
   /* double */
   double f;
   for(f = 1; 1+f != 1; f/=2){}
   f *= 2;
   printf("eps double = %.10lg\n", f);
  
   /* long double */
   long double g;
   for(g = 1; 1+g != 1; g/=2){}
   g *= 2;
   printf("eps long   = %.10Lg\n", g);    


   printf("\nMachine precision using do loop:\n");

   /* float */
   float a = 1;
   do{ a/=2; }
   while( 1+a != 1 );
   a *= 2;
   printf("eps float  = %.10g\n", a);
   
   /* double */
   double b = 1;
   do{ b/=2; }
   while( 1+b != 1 );
   b *= 2;
   printf("eps double = %.10lg\n", b);
  
   /* long double */
   long double c = 1;
   do{ c/=2; }
   while( 1+c != 1 );
   c *= 2;
   printf("eps long   = %.10Lg\n", c);


   // Print machine epsilong from system 
   printf("\nMachine precision from system\n");
   printf("eps float  = %.10g\n", FLT_EPSILON);
   printf("eps double = %.10lg\n", DBL_EPSILON);
   printf("eps long   = %.10Lg\n", LDBL_EPSILON);



   /***************************************************************************
    * TASK 2 -- order of summation
    **************************************************************************/

   /***** TASK 2 (i) -- harmonic series *****/
   printf("\n\nTASK 2 (i): HARMONIC SERIES\n");

   // Define and print a large integer
   printf("This is some large number:\n");
   int max = INT_MAX/3;
   printf("max = %d\n", max);

   // Compute and print truncated harmonic series in the 'upwards' direction 
   float sum_up_float = 0;
   for(int i = 1; i <= max; i++){
      sum_up_float += 1.0f/i;
   }
   printf("sum_up_float    = %.16g\n", sum_up_float);

   // Compute and print truncated harmonic series in the 'downwards' direction 
   float sum_down_float = 0;
   for(int i = max; i > 0; i--){
      sum_down_float += 1.0f/i;
   }

   printf("sum_down_float  = %.16g\n", sum_down_float);


   /***** TASK 2 (ii) -- explain the difference *****/
   printf("\n\nTASK 2 (ii): EXPLAIN THE DIFFERENCE\n");
   printf("When summing up the first terms are large, so at the end of the sum,\n");
   printf("the small terms that are added are way outside precision.\n");
   printf("When summing down the first terms are small and can be 'detected' within precision.\n");
   printf("Thus the 'up' sum will be smaller than the 'down' sum.\n");
  

   /***** TASK 2 (iii) -- does the sum converge? *****/
   printf("\n\nTASK 2 (iii): DOES THE SUM CONVERGE?\n");
   printf("No, the harmonic series is divergent (it is easily shown to be\n");
   printf("greater than 1/2 + 1/2 + ... = infty)\n");


   /***** TASK 2 (iv) -- repeat with double *****/
   printf("\n\nTASK 2 (iv): REPEAT WITH DOUBLE\n");
   
   // Compute and print truncated harmonic series in the 'upwards' direction 
   double sum_up_double = 0;
   for(int i = 1; i <= max; i++){
      sum_up_double += 1.0/i;
   }
   printf("sum_up_double   = %.16lg\n", sum_up_double);

   // Compute and print truncated harmonic series in the 'downwards' direction 
   double sum_down_double = 0;
   for(int i = max; i > 0; i--){
      sum_down_double += 1.0/i;
   }
   printf("sum_down_double = %.16lg\n", sum_down_double);

   printf("Now there is almost no difference. Precision is sufficient to capture\n");
   printf("the small terms, even when they are added to something large.");
  
  
  /***************************************************************************
    * TASK 3 -- equal
   **************************************************************************/
   printf("\n\nTASK 3: EQUAL\n");

   // Absolute and relative precision
   printf("Check whether numbers are equal:\n");
   double tau = 1e-8;
   double epsilon = 1e-4;
   
   double A = 1.000000000000;
   double B = 1.000000000009;
   
   printf("\na = %.20f\n", A);
   printf("b = %.20f\n", B);
   if ( equal(A, B, tau, epsilon) == 1 ) {
      printf("a and b are equal with absolute precision tau = %g and relative precision eps = %g!\n", tau, epsilon);
   } 
   else {
      printf("a and b are not equal with absolute precision tau = %g and relative precision eps = %g!\n", tau, epsilon);
   }
   
   A = 1.000;
   B = 1.009;
   
   printf("\na = %.20f\n", A);
   printf("b = %.20f\n", B);
   if ( equal(A, B, tau, epsilon) == 1 ) {
      printf("a and b are equal with absolute precision tau = %g and relative precision eps = %g!\n", tau, epsilon);
   } 
   else {
      printf("a and b are not equal with absolute precision tau = %g and relative precision eps = %g!\n", tau, epsilon);
   }

   /***************************************************************************
    * TASK 4 -- name digit
   **************************************************************************/   
   
   printf("\n\nTASK 4: NAME DIGIT\n");

   printf("\nname_digit(7):\n");
   name_digit(7);
   printf("\nname_digit(4):\n");
   name_digit(4);
   printf("\nname_digit(99):\n");
   name_digit(99);

   return 0;
}

