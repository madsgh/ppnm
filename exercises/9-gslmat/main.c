#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_eigen.h>

void vector_print(gsl_vector* vec){

	for(int i = 0; i < vec->size; i++) {
      printf("%10.4f ", gsl_vector_get(vec,i));
   }
	printf("\n");

}

void matrix_print(gsl_matrix* mat){

	for(int i = 0; i < mat->size1; i++) {
	   for(int j = 0; j < mat->size2; j++) {
         printf("%10.4f ", gsl_matrix_get(mat, i, j));
      }
	   printf("\n");
   }

}


int main() {
   
   //
   // TASK A
   //

   printf("\n\nTASK A\n");

   // size of matrix
   int n = 3;

   // initialize matrices and vectors for solving A*x = b
   gsl_matrix* Aorig = gsl_matrix_alloc(n,n); // original matrix A
	gsl_matrix* Acopy = gsl_matrix_alloc(n,n); // copy of A
	gsl_vector* b = gsl_vector_alloc(n);       // RHS vector
	gsl_vector* x = gsl_vector_alloc(n);       // vector for storing x
	gsl_vector* y = gsl_vector_calloc(n);      // vector for storing A*x (for checking solution)

   // set elements of A
   gsl_matrix_set(Aorig, 0, 0,  6.13); 
   gsl_matrix_set(Aorig, 0, 1, -2.90); 
   gsl_matrix_set(Aorig, 0, 2,  5.86); 
   gsl_matrix_set(Aorig, 1, 0,  8.08); 
   gsl_matrix_set(Aorig, 1, 1, -6.31); 
   gsl_matrix_set(Aorig, 1, 2, -3.89); 
   gsl_matrix_set(Aorig, 2, 0, -4.36); 
   gsl_matrix_set(Aorig, 2, 1,  1.00); 
   gsl_matrix_set(Aorig, 2, 2,  0.19); 

   // copy A
   gsl_matrix_memcpy(Acopy, Aorig);

   // set elements of b
   gsl_vector_set(b, 0, 6.23);
   gsl_vector_set(b, 1, 5.37);
   gsl_vector_set(b, 2, 2.29);

   // solve system
   gsl_linalg_HH_solve(Acopy,b,x);

   // compute A*x to check solution (y = 1*A*x + 0*y = A*x)
   gsl_blas_dgemv(CblasNoTrans, 1, Aorig, x, 0, y);

   // check solution
   printf("Check that we have actually solved A*x = b!\n");

   printf("b =\n");
   vector_print(b);

   printf("A*x =\n");
   vector_print(y);

   // free memory
   gsl_matrix_free(Aorig);
   gsl_matrix_free(Acopy);
   gsl_vector_free(b);
   gsl_vector_free(x);
   gsl_vector_free(y);


   //
   // TASK B
   //

   printf("\n\nTASK B\n");
   
   // size of matrix
   int m = 4;

   // initialize matrices and vectors
	gsl_matrix* Horig  = gsl_matrix_alloc(m,m); // matrix to hold Hilbert matrix (real symmetric)
	gsl_matrix* Hcopy  = gsl_matrix_alloc(m,m); // matrix to hold copy of Horig
	gsl_matrix* eigvec = gsl_matrix_alloc(m,m); // matrix to hold eigenvectors as columns
	gsl_vector* eigval = gsl_vector_alloc(m);   // vector to hold eigenvalues
	gsl_vector* column = gsl_vector_alloc(m);   // vector to hold an extracted column
	gsl_vector* matrix_times_eigvec = gsl_vector_alloc(m);   // vector to hold matrix*eigenvector which should be equal to eigenvalue*eigenvector

   // set elements of Horig
	for(int i = 0; i < Horig->size1; i++) {
		for(int j = 0; j < Horig->size2; j++) {
		   gsl_matrix_set(Horig,i,j, 1.0/(i + j + 1));
		}
   }

   // copy Horig into Hcopy
   gsl_matrix_memcpy(Hcopy, Horig);

   // print matrix to check that it is correct
   printf("We are trying to diagonalize this matrix:\n");
   printf("H =\n");
   matrix_print(Horig);

   // allocate memory for performing diagonalization (H is real symmetric)
   gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(m);

   // run the diagonalization using Hcopy (which is partially destroyed)
   gsl_eigen_symmv(Hcopy, eigval, eigvec, w);

   // loop over eigenvectors and chech that they are correct
   printf("\nCheck that we have correctly diagonalized H:\n");
   for (int i = 0; i < m; i++) {

      // extract eigenvectors
      gsl_matrix_get_col(column, eigvec, i);
      
      gsl_blas_dgemv(CblasNoTrans, 1, Horig, column, 0, matrix_times_eigvec);

      // print matrix*eigenvector and compare to eigenvalue*eigenvector
      printf("\nmatrix*eigenvector =\n");
      vector_print(matrix_times_eigvec);
      
      gsl_vector_scale(column, gsl_vector_get(eigval,i));
      printf("eigenvalue*eigenvector =\n");
      vector_print(column);
      

   }
   

   // free memory
   gsl_eigen_symmv_free(w);
   gsl_matrix_free(Horig);
   gsl_matrix_free(Hcopy);
   gsl_matrix_free(eigvec);
   gsl_vector_free(eigval);
   gsl_vector_free(column);
   gsl_vector_free(matrix_times_eigvec);
   return 0;
}
