#include<math.h>
#include<complex.h>

// single precision complex gamma function (Gergo Nemes, from Wikipedia)
complex double mycomplexgamma(double complex x) {
   if (creal(x) < 0) return M_PI/csin(M_PI*x)/mycomplexgamma(1-x);
   if (creal(x) < 9) return mycomplexgamma(x+1)/x;
   complex double ln_gamma = x*clog(x+1/(12*x-1/x/10))-x+clog(2*M_PI/x)/2;
   return cexp(ln_gamma);
}
