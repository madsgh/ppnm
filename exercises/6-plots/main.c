#include<stdio.h>
#include<math.h>
#include<complex.h>
#include<gsl/gsl_sf_erf.h>
#include<gsl/gsl_sf_gamma.h>

// declare homemade functions
double myerf(double);
double mygamma(double);
double mylngamma(double);
complex double mycomplexgamma(complex double);

// include factorial which is implemented in factorial.c
int factorial(int);

// main function
int main(){

	// error function
   	FILE* erf_file = fopen("data_erf.txt", "w");
   	double x_min =-2;
   	double x_max = 2;
	for (double x = x_min; x <= x_max; x += 1.0/8) {
		fprintf(erf_file, "%10.6f %10.6f %10.6f %10.6f\n", x, erf(x), gsl_sf_erf(x), myerf(x));
	}

  	 // gamma function
   	FILE* gamma_file = fopen("data_gamma.txt", "w");
	x_min =-5.0 + 1.0/128;
   	x_max = 5.0;
	for (double x = x_min; x <= x_max; x += 1.0/64) {
		fprintf(gamma_file, "%12.6f %12.6f %12.6f %12.6f\n", x, tgamma(x), gsl_sf_gamma(x), mygamma(x));
	}

   	FILE* tab_gamma_file = fopen("tabulated_gamma.txt", "w");
	for (int n = 1; n <= 5; n++) {
		fprintf(tab_gamma_file, "%4d %4d\n", n, factorial(n - 1));
	}

   	// logarithm of absolute value of gamma function
   	FILE* lngamma_file = fopen("data_lngamma.txt", "w");
	x_min =-5.0 + 1.0/128;
   	x_max = 5.0;
	for (double x = x_min; x <= x_max; x += 1.0/64) {
		fprintf(lngamma_file, "%12.6f %12.6f %12.6f %12.6f\n", x, lgamma(x), gsl_sf_lngamma(x), mylngamma(x));
	}

   	FILE* tab_lngamma_file = fopen("tabulated_lngamma.txt", "w");
	for (int n = 1; n <= 5; n++) {
		fprintf(tab_lngamma_file, "%4d %10.6f\n", n, log(factorial(n - 1)));
	}

   	// absolute value of complex gamma function
   	FILE* complexgamma_file = fopen("data_complexgamma.txt", "w");
	double re_min =-5.0 + 1.0/128;
   	double re_max = 5.0;
	double im_min =-5.0 + 1.0/128;
   	double im_max = 5.0;
	for (double re = re_min; re <= re_max; re += 1.0/16) {
	   for (double im = im_min; im <= im_max; im += 1.0/16) {
			fprintf(complexgamma_file, "%12.6f %12.6f %12.6f\n", re, im, cabs(mycomplexgamma(re + I*im)));
        }
	}

	fclose(erf_file);
	fclose(gamma_file);
	fclose(tab_gamma_file);
	fclose(tab_lngamma_file);
	fclose(complexgamma_file);
	return 0;
}
