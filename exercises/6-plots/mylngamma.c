#include<math.h>

// single precision ln(gamma) function (Gergo Nemes, from Wikipedia)
double mylngamma(double x) {
   if (x<0) return log(M_PI) - log(fabs(sin(M_PI*x))) - mylngamma(1-x);
   if (x<9) return mylngamma(x+1) - log(x);
   double ln_gamma = x*log(x+1/(12*x-1/x/10))-x+log(2*M_PI/x)/2;
   return ln_gamma;
}
