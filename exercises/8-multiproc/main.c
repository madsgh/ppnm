#include<stdio.h>
#include<math.h>
#include<pthread.h>
#include<stdlib.h>
#include<string.h>

// define struct to hold input and output parameters
typedef struct {
   int N;    // total numbe of points
   int n;    // number of inside points
   unsigned int seed; // seed for rand_r()
   char* out_file_string;  // string to make output file names
} counter;


// wrapper function to do the job
void* wrapper(void* arg) {

   // extract parameters
   counter* count = (counter*)arg;
   int N = count->N;
   int n = count->n;
   unsigned int seed = count->seed;
   char file_name_inside[100] = "";  strcat(file_name_inside , "inside_");  strcat(file_name_inside , count->out_file_string);
   char file_name_outside[100] = ""; strcat(file_name_outside, "outside_"); strcat(file_name_outside, count->out_file_string);
   
   //fprintf(stdout, "%s\n", file_name_inside);
   //fprintf(stdout, "%s\n", file_name_outside);

   // initialize x,y
   double x, y;
      
   // open files to write points 
   FILE* file_inside = fopen(file_name_inside, "w");
   FILE* file_outside = fopen(file_name_outside, "w");

   // loop over N random points
   for (int i = 1; i <= N; i++) {

      // generate and print points
      x = ((double)rand_r(&seed))/RAND_MAX; // ensure double (rand_r returns int)
      y = ((double)rand_r(&seed))/RAND_MAX; // ensure double (rand_r returns int)
     
      // check whether point is inside
      if (sqrt(x*x + y*y) <= 1.0) {
         n += 1;
         fprintf(file_inside, "%10.6f %10.6f\n", x, y);
      } else {
         fprintf(file_outside, "%10.6f %10.6f\n", x, y);
      }

   }
   
   // write number of inside point to argument struct
   count->n = n;

   // close files
   fclose(file_inside);
   fclose(file_outside);

   // return nothing because result is written to to arg
   return NULL;
}


int main() {

   // counters (plotting is SLOW for large N)
   counter count_1 = {.N = 1e4, .n=0, .seed=101, .out_file_string="points_1.txt"};
   counter count_2 = {.N = 1e4, .n=0, .seed=202, .out_file_string="points_2.txt"};

   // run one loop in a new thread
   pthread_t thread;
   pthread_create(&thread, NULL, wrapper, (void*)&count_1); // NULL means default attributes
   
   // run one loop in main thread
   wrapper( (void*)&count_2 );

   // join thread
   pthread_join(thread, NULL); // send return value to NULL

   // total number of inside and outside points
   double n_tot = (double)(count_1.n + count_2.n);
   double N_tot = (double)(count_1.N + count_2.N);
   double pi = 4*n_tot/N_tot;

   printf("pi = %.6f using pthreads and N = %.2e\n", pi, N_tot);

   return 0;
}
