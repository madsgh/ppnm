pi = 3.196000 using OpenMP and N = 1.00e+03, signed error = +0.0544073464
pi = 3.158800 using OpenMP and N = 1.00e+04, signed error = +0.0172073464
pi = 3.143080 using OpenMP and N = 1.00e+05, signed error = +0.0014873464
pi = 3.142944 using OpenMP and N = 1.00e+06, signed error = +0.0013513464
pi = 3.141870 using OpenMP and N = 1.00e+07, signed error = +0.0002773464
pi = 3.141865 using OpenMP and N = 1.00e+08, signed error = +0.0002724264
pi = 3.141593 using OpenMP and N = 1.00e+09, signed error = +0.0000001464
