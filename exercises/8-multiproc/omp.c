#include<stdio.h>
#include<math.h>
#include<omp.h>
#include<stdlib.h>


int loop(int N, unsigned int seed) {

   // set counter
   int n = 0;

   // declare x, y
   double x, y;

   // loop over N random points
   for (int i = 1; i <= N; i++) {

      // generate and print points
      x = ((double)rand_r(&seed))/RAND_MAX; // ensure double (rand_r returns int)
      y = ((double)rand_r(&seed))/RAND_MAX; // ensure double (rand_r returns int)
     
      // check whether point is inside
      if (sqrt(x*x + y*y) <= 1.0) {
         n += 1;
      }

   } 
   
   return n;

} /* end of loop function */


int main() {
   
   // open file to write error
   FILE* error_file = fopen("pi_error.txt", "w");

   // set of N that we want to test
   int N_array[] = {1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9};

   // loop over different N in N_array
   for (int i = 0; i <= 6; i++) {

      // set number of points and declare output variables
      int N = N_array[i]/2; // we are running on TWO threads
      int n_1, n_2;

      #pragma omp parallel sections 
      {
         #pragma omp section
         {
         unsigned int seed_1 = 101;
         n_1 = loop(N, seed_1);
         //printf("Hello from thread %d, nthreads %d\n", omp_get_thread_num(), omp_get_num_threads());
         }
         #pragma omp section
         {
         unsigned int seed_2 = 202;
         n_2 = loop(N, seed_2);
         //printf("Hello from thread %d, nthreads %d\n", omp_get_thread_num(), omp_get_num_threads());
         }
      }
      
      // calculate answer
      double N_tot = (double)(2*N);
      double n_tot = (double)(n_1 + n_2);
      double pi = 4*n_tot/N_tot;
      printf("pi = %.6f using OpenMP and N = %.2e, signed error = %+.10f\n", pi, N_tot, pi - M_PI);
      fprintf(error_file, "%10g %+.10f\n", N_tot, pi - M_PI);

   }
   
   fclose(error_file);
   return 0;
}
