#include<stdio.h>
#include<math.h>
#include<complex.h>

int main(){
  
   // Calculate and print some stuff
   float x1 = tgamma(5);
   printf("gamma(5) = %g\n", x1);
   
   float x2 = j1(0.5);
   printf("J1(0.5) = %g\n", x2);
   
   complex double x3 = csqrt(-2);
   printf("sqrt(-2) = %g + i*%g\n", creal(x3), cimag(x3));

   complex double x4 = cexp(I*M_PI);
   printf("exp(i*pi) = %g + i*%g\n", creal(x4), cimag(x4));
 
   complex double x5 = cexp(I);
   printf("exp(i) = %g + i*%g\n", creal(x5), cimag(x5));

   complex double x6 = cpow(I, M_E);
   printf("i**e = %g + i*%g\n", creal(x6), cimag(x6));

   complex double x7 = cpow(I, I);
   printf("i**i = %g + i*%g\n", creal(x7), cimag(x7));
   

   // Check precision of types
   float x_float = 1.f/9;
   double x_double = 1./9;
   long double x_longdouble = 1.L/9;

   printf("x_float      = %.25g\n", x_float);
   printf("x_double     = %.25lg\n", x_double);
   printf("x_longdouble = %.25Lg\n", x_longdouble);


   return 0;
}
